package Centre;
import java.awt.Image;

import javax.imageio.ImageIO;

//import all images here

public class ImportImages { // import images 
	
	/*PLAYER SPRITE 1*/
	public static Image player; // make image
	public static Image player1; // player image left (approx 40 deg)
	public static Image player2; // player image left (approx 30 deg)
	public static Image player3; // player image left (approx 20 deg)
	public static Image player4; // player image left (approx 10 deg)
	public static Image player6; // player image right (approx 10 deg)
	public static Image player7; // player image right (approx 20 deg)
	public static Image player8; // player image right (approx 30 deg)
	public static Image player9; // player image right (approx 40 deg)
	
	/*PLAYER SPRITE 2*/
	public static Image secondplayer1; // player2 image right
	public static Image secondplayer2; // player2 image right
	public static Image secondplayer3; // player2 image right
	public static Image secondplayer4; // player2 image right
	public static Image secondplayer5; // player2 image right
	public static Image secondplayer6; // player2 image right
	public static Image secondplayer7; // player2 image right
	public static Image secondplayer8; // player2 image right
	public static Image secondplayer9; // player2 image right
	public static Image secondplayer10; // player2 image right
	public static Image secondplayer11; // player2 image centre
	public static Image secondplayer12; // player2 image left
	public static Image secondplayer13; // player2 image left
	public static Image secondplayer14; // player2 image left
	public static Image secondplayer15; // player2 image left
	public static Image secondplayer16; // player2 image left
	public static Image secondplayer17; // player2 image left
	public static Image secondplayer18; // player2 image left
	public static Image secondplayer19; // player2 image left
	public static Image secondplayer20; // player2 image left
	public static Image secondplayer21; // player2 image left
	
	public static Image missile;
	public static Image planetbackground;
	public static Image enemy;
	public static Image controls;
	public static Image shield;
	public static Image upgradebuilding;
	public static Image galaxy;
	public static Image coin;
	public static Image superalien;
	public static Image enemy3;
	public static Image multishot;
	public static Image victory;
	public static Image defeat;
	public static Image multishotwarning;
	public static Image multishotnone;
	public static Image enemymissile;
	public static Image survival;
	
	public ImportImages() {
		try {
			player = ImageIO.read(getClass().getResource("/redfighter0005.png")); // import player, free game resource taken from http://opengameart.org/content/animated-red-ship
			player1 = ImageIO.read(getClass().getResource("/redfighter0001.png")); // player left, free game resource taken from http://opengameart.org/content/animated-red-ship
			player2 =  ImageIO.read(getClass().getResource("/redfighter0002.png")); // player left, free game resource taken from http://opengameart.org/content/animated-red-ship
			player3 =  ImageIO.read(getClass().getResource("/redfighter0003.png")); // player left, free game resource taken from http://opengameart.org/content/animated-red-ship
			player4 =  ImageIO.read(getClass().getResource("/redfighter0004.png")); // player left, free game resource taken from http://opengameart.org/content/animated-red-ship
			player6 = ImageIO.read(getClass().getResource("/redfighter0006.png")); // player right, free game resource taken from http://opengameart.org/content/animated-red-ship
			player7 =  ImageIO.read(getClass().getResource("/redfighter0007.png")); // player right, free game resource taken from http://opengameart.org/content/animated-red-ship
			player8 =  ImageIO.read(getClass().getResource("/redfighter0008.png")); // player right, free game resource taken from http://opengameart.org/content/animated-red-ship
			player9 =  ImageIO.read(getClass().getResource("/redfighter0009.png")); // player right, free game resource taken from http://opengameart.org/content/animated-red-ship
			secondplayer11 = ImageIO.read(getClass().getResource("/nave0011.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer1 = ImageIO.read(getClass().getResource("/nave0001.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer2 = ImageIO.read(getClass().getResource("/nave0002.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer3 = ImageIO.read(getClass().getResource("/nave0003.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer4 = ImageIO.read(getClass().getResource("/nave0004.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer5 = ImageIO.read(getClass().getResource("/nave0005.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer6 = ImageIO.read(getClass().getResource("/nave0006.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer7 = ImageIO.read(getClass().getResource("/nave0007.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer8 = ImageIO.read(getClass().getResource("/nave0008.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer9 = ImageIO.read(getClass().getResource("/nave0009.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer10 = ImageIO.read(getClass().getResource("/nave0010.png")); // player 2 right http://opengameart.org/content/little-spaceship
			secondplayer12 = ImageIO.read(getClass().getResource("/nave0012.png")); // player 2 left http://opengameart.org/content/little-spaceship
			secondplayer13 = ImageIO.read(getClass().getResource("/nave0013.png")); // player 2 left http://opengameart.org/content/little-spaceship
			secondplayer14 = ImageIO.read(getClass().getResource("/nave0014.png")); // player 2 left http://opengameart.org/content/little-spaceship
			secondplayer15 = ImageIO.read(getClass().getResource("/nave0015.png")); // player 2 left http://opengameart.org/content/little-spaceship
			secondplayer16 = ImageIO.read(getClass().getResource("/nave0016.png")); // player 2 left http://opengameart.org/content/little-spaceship
			secondplayer17 = ImageIO.read(getClass().getResource("/nave0017.png")); // player 2 left http://opengameart.org/content/little-spaceship
			secondplayer18 = ImageIO.read(getClass().getResource("/nave0018.png")); // player 2 left http://opengameart.org/content/little-spaceship
			secondplayer19 = ImageIO.read(getClass().getResource("/nave0019.png")); // player 2 left http://opengameart.org/content/little-spaceship
			secondplayer20 = ImageIO.read(getClass().getResource("/nave0020.png")); // player 2 left http://opengameart.org/content/little-spaceship
			secondplayer21 = ImageIO.read(getClass().getResource("/nave0021.png")); // player 2 left http://opengameart.org/content/little-spaceship
			missile = ImageIO.read(getClass().getResource("/missile.png"));
			enemy = ImageIO.read(getClass().getResource("/Alien.png"));
			planetbackground = ImageIO.read(getClass().getResource("/Caldonia.jpg")); // free resources http://www.spiralgraphics.biz/packs/planet/previews/Caldonia.jpg
			galaxy = ImageIO.read(getClass().getResource("/BonusRoundBackground.png")); //http:ftopics.com/galaxy-desktop-background/galaxy-desktop-background-2/
			enemy = ImageIO.read(getClass().getResource("/Alien.png")); //replace path with enemy.png
			controls = ImageIO.read(getClass().getResource("/control.png"));
			superalien = ImageIO.read(getClass().getResource("/SuperAlien.png"));
			enemy3 = ImageIO.read(getClass().getResource("/enemyblue.png"));
			upgradebuilding = ImageIO.read(getClass().getResource("/tech_room_space_station_bump_mapped_example.jpg")); // free background taken from http://www.geminoidi.com/images/textures/tech_room_space_station_bump_mapped_example.jpg
			multishot = ImageIO.read(getClass().getResource("/multishot.png"));
			victory = ImageIO.read(getClass().getResource("/Victory.png"));
			defeat = ImageIO.read(getClass().getResource("/Defeat.png"));
			multishotwarning = ImageIO.read(getClass().getResource("/multishotwarning.png"));
			multishotnone = ImageIO.read(getClass().getResource("/multishotnone.png"));
			enemymissile = ImageIO.read(getClass().getResource("/enemymissile.png"));
			survival = ImageIO.read(getClass().getResourceAsStream("/survival.png"));
		}catch (Exception error) {
			error.printStackTrace(); // report error
		}
	}

}
