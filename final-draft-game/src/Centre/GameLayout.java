package Centre;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import StateMachine.StateMachineControl;



public class GameLayout extends JPanel implements Runnable, KeyListener, MouseListener, MouseMotionListener {
	
	/*
	 *GameLayout.java defines the gamescreen dimensions. 
	 */
	private static final long serialVersionUID = 1L;
	
	public static int GAME_WIDTH = 800;
	public static int GAME_HEIGHT = 600;
	Thread thread;
	static int gameonoff = 0;
	StateMachineControl smc;
	
	/*Constructor*/
	public GameLayout() {
		setPreferredSize(new Dimension(GAME_WIDTH, GAME_HEIGHT));
		addKeyListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		setFocusable(true);
		 smc = new StateMachineControl();
		start();
	}
	
	public synchronized void start() { // check if the system is started
		gameonoff = 1;
		thread = new Thread(this); // initiate the thread
		thread.start();	// start the thread
		
	}
	
	public synchronized void stop() { // check if the system is off
		gameonoff = 0;
		try {
			thread.join(); // kill the thread (no more game)
		} catch (InterruptedException error) { // get error message
			error.printStackTrace();
		}
		
	}
	 
	  
	 /*Game loop
	  * This is where the thread will be running
	  */
	  public void run() {

		// frames counter
		while(gameonoff == 1) { // when the system is on
			
			control(); // initialise the video game in the loop (thread)
			repaint(); // component repaints itself (as in refreshes)
			
			 try {
				 Thread.sleep(16); // control game speed here (16ms delay)
			 } catch (InterruptedException error) {
				 error.printStackTrace();
			 }

		}
		
	}
	
	public void control() { // command logic goes here
		smc.control();
	}


	public void paint(Graphics gfx) { // choose the colour of the game

		gfx.setColor(Color.BLACK); // black background
		gfx.fillRect(0,0,GAME_WIDTH,GAME_HEIGHT); // fill the background		
		
		paintStateVisuals(gfx); //private method for displaying individual states
	}
	
	

	public void keyPressed(KeyEvent key) {
		int keycode;
		keycode = key.getKeyCode();
		smc.keyPressed(keycode); // input key but convert it to int value
	}


	public void keyReleased(KeyEvent key) {
		int keycode;
		keycode = key.getKeyCode();
		smc.keyReleased(keycode);
	}

	public void mouseMoved(MouseEvent mouse) {
		smc.mouseMoved(mouse);
		
	}
	
	public void mouseClicked(MouseEvent mouse) {
		smc.mouseClicked(mouse);
		
	}
	
	/*Private Methods*/
	private void paintStateVisuals(Graphics gfx){
		if (StateMachineControl.outputmainmenucontrol == 0) {
			smc.mainmenu.paint_mainmenu(gfx);
		} else if (StateMachineControl.outputmainmenucontrol == 1) {
			smc.camp1.paint_camp(gfx);
		} else if (StateMachineControl.outputmainmenucontrol == 2) {
			smc.option.paint_Options(gfx);
		} else if (StateMachineControl.outputmainmenucontrol == 3) {
			smc.winscreen.paint_winscreen(gfx);
		} else if (StateMachineControl.outputmainmenucontrol == 4) {
			smc.losescreen.paint_losescreen(gfx);
		} else if (StateMachineControl.outputmainmenucontrol == 5) {
			smc.upgrademenu.Paint_Upgrade(gfx);
		} else if (StateMachineControl.outputmainmenucontrol == 6) {

		} else if (StateMachineControl.outputmainmenucontrol == 7) {
			smc.survivalpreparescreen.paint_survivalpreparescreen(gfx);
		} else if (StateMachineControl.outputmainmenucontrol == 8) {
			smc.survivalsolo.paint_survivalsolo(gfx);
		} else if (StateMachineControl.outputmainmenucontrol == 9) {
			smc.survivalco_op.paint_survivalcoop(gfx);
		} else if (StateMachineControl.outputmainmenucontrol == 10) {
			smc.cpudemo.paint_CPU(gfx);
		}
	}

	/*START: UNUSED METHODS FROM INTERFACES*/
	public void mouseEntered(MouseEvent mouse) {
		// TODO Auto-generated method stub
		
	}

	public void mouseExited(MouseEvent mouse) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent mouse) {
		// TODO Auto-generated method stub
		
	}

	public void mouseReleased(MouseEvent mouse) {
		// TODO Auto-generated method stub
		
	}
	public void keyTyped(KeyEvent key) {
		// TODO Auto-generated method stub
		
	}

	public void mouseDragged(MouseEvent mouse) {
		// TODO Auto-generated method stub
		
	}
	/*UNUSED METHODS FROM INTERFACE*/

}
