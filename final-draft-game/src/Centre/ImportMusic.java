package Centre;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class ImportMusic {
	/*
	 * This class will handle all music/sound file imports
	 */
	
	private Clip file; // make clip for audiodata that can be played
	// wav files
	
	public ImportMusic(String url) {
		
		try {
			AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(url));  // import audio file from the resources
			file = AudioSystem.getClip(); // get the files so the wav can be played
			file.open(audio); 
		} catch (Exception error) {
			error.printStackTrace(); 
		}
	}
	
	public void close() { // close the wav file
		stop();	
		file.close();
	}
		
	
	public void stop() {
		if (file.isRunning() == true) { // if the wav file is running
			file.stop();
		} else {
			
		}
	}
	
	
	public void playwav(){		// play the wav file
		if (file == null) { 
			return;
		}
		stop(); // play it once and stop
		file.setFramePosition(0); // location of the sound (in the 0 frame (base))
		file.start(); // start the clip sound
		
	}

}
