package Centre;

import javax.swing.JFrame;

public class centreframe { // start JFRAME
	/*
	 * This class instantiates the JFrame and calls GameLayout.java
	 */
	
	public static void main(String[] args) {
		new ImportImages(); // can now import images
		JFrame frame = new JFrame("Space Invaders");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false); // cannot resize
		frame.setContentPane(new GameLayout()); //inputs things into the frame
		frame.pack(); // makes the window
		frame.setLocationRelativeTo(null);
		frame.setVisible(true); // visible to the game
		frame.setIconImage(ImportImages.enemy);
	}
}