package StateMachine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import Centre.ImportImages;
import Model.PlayerControl;

public class UpgradeMenu implements StateInterface{

	public StateMachineControl smc;
	String playermoney; // since we are going to output data as string (to the screen)
	String missileupgrade, multimissileupgrade;
	String playerlives;
	String[] commands = {"Upgrade Railgun: ", "Purchase 5Shot: ", "Lives: ", "Return to Game"};
	int cursorinput = 0;
	int checkmoney = 0;// used to confirm the upgrade transaction etc
	int lifemoney = 0;
	private int updatestatus = 0; 
	int multiupgrademoney = 0;
	int missileupgrademax = 3; // max number of upgrades
	int multimissilepurchase;
	int multimissilepurchasenumber = 99;
	int missileupgradenumber; 
	public int money, cheatmoney,upgrademoney; // type of money
	int campaginmoney;
	
	//constructor
	
	public UpgradeMenu(StateMachineControl stateMachineControl) {
		this.smc = stateMachineControl;
		
		if (updatestatus == 0) {
			update();
		}
		startoff(); // initiaise variables
	}
	
	public void startoff() {

		campaginmoney = Campagin1_1.money;
		money = smc.money + campaginmoney + cheatmoney + upgrademoney;
		System.out.println("Campagin: " + campaginmoney);
		playermoney = new Integer(money).toString(); //  convert data to string		
		missileupgradenumber = smc.missileupgrade; // link data of upgrades to global
		multimissilepurchase = smc.multimissileupgrade;
		missileupgrade = new Integer(missileupgradenumber).toString();
		multimissileupgrade = new Integer(multimissilepurchase).toString();
		playerlives = Campagin1_1.playercontrol.getLivesAsString();
		// TODO Auto-generated method stub
		
	}

	public void update() {
		updatestatus = 1;
		upgrade();
	}
	public void control() {
		startoff();
		
	}


	public void keyPressed(int keycode) {
		int length = commands.length;
		int down = KeyEvent.VK_DOWN;
		int up = KeyEvent.VK_UP;
		int enter = KeyEvent.VK_ENTER;
		int cheat = KeyEvent.VK_T;
		//int 
		if (updatestatus == 0) {
			
		}
		if (keycode == cheat) {
			cheatmoney = cheatmoney + 100000; // money cheat !!!!!
			System.out.println(money);
			startoff(); // refresh the data outputted by spending the cash
		} else if (keycode == KeyEvent.VK_U) {
			smc.changeoutputmainmenucontrol(1); // return to the game (probably remove this later on)
		}
		if (keycode == down) {
			cursorinput = cursorinput + 1; // for upgrade menu implementation
			lifemoney = 0; // reset the data variables
			checkmoney = 0;
			if (cursorinput >= length) {
				cursorinput = 0;
			}
		} else if (keycode == up) {
			cursorinput = cursorinput - 1;
			lifemoney = 0; // reset the data variables
			checkmoney = 0;
			if (cursorinput < 0) {
				cursorinput = length - 1;
			}
		} else if (keycode == enter) {
			upgrade();
		}
		// TODO Auto-generated method stub
		
	}


	public void keyReleased(int keycode) {
		// TODO Auto-generated method stub
		
	}


	public void mouseMoved(MouseEvent mouse) {
		int mousexpos = mouse.getX();// get xpos
		int mouseypos = mouse.getY();// get ypos
		// get area where the mouse cursor impacts the mainmenu controls
		if ((mousexpos > 50) && (mousexpos < 200) && (mouseypos > 180) && (mouseypos < 210) ) { 
			cursorinput = 0;
		} else if ((mousexpos > 50) && (mousexpos < 200) && (mouseypos > 210) && (mouseypos < 250)){
			cursorinput = 1;
		} else if ((mousexpos > 50) && (mousexpos < 200) && (mouseypos > 250) && (mouseypos < 290)) {
			cursorinput = 2;
		} else if ((mousexpos > 50) && (mousexpos < 200) && (mouseypos > 290) && (mouseypos < 330)){
			cursorinput = 3;
		} else {
			cursorinput = 4;
			lifemoney = 0;
			checkmoney = 0;
		}
		// TODO Auto-generated method stub
		
	}


	public void mouseClicked(MouseEvent mouse) {
		int output = MouseEvent.BUTTON1;
		if (output == MouseEvent.BUTTON1) { // upgrade when clicked
			upgrade();
		}
		
		// TODO Auto-generated method stub
		
	}
	
	public void upgrade() {
		
		if (cursorinput == 0) {
			
			if (smc.missileupgrade < missileupgrademax) { // can upgrade
				if (money >= 1000) { // enough money
					upgrademoney = upgrademoney - 1000; // complete the transaction
					checkmoney = 2;
					smc.missileupgrade(1);
					System.out.println(money);
				} else if (money < 1000) { // not enough money
					checkmoney = 1;
					smc.missileupgrade(0);
				} 
			} else if (smc.missileupgrade >= missileupgrademax) {
				checkmoney = 3;
				smc.missileupgrade(0);
			}

		
		} else if (cursorinput == 2) { // get more lives
			
			if (money >= 10000) {
				upgrademoney = upgrademoney - 10000;
				lifemoney = 2;
				Campagin1_1.playercontrol.updatelives(1);
			}  else if (money < 10000) { // not enough money
				lifemoney = 1;
				Campagin1_1.playercontrol.updatelives(0);
			}

		}  else if (cursorinput == 1) {
			if (smc.multimissileupgrade < multimissilepurchasenumber) {
				if (money >= 1000) {
					upgrademoney = upgrademoney - 1000;
					multiupgrademoney = 2;
					smc.multimissileupgrade(1);
				} else if (money < 1000) {
					multiupgrademoney = 1;
					smc.multimissileupgrade(0);
					
				}
			} else if (smc.multimissileupgrade >= multimissilepurchasenumber) {
				multiupgrademoney = 3;
				smc.multimissileupgrade(0);
			}
		} else if (cursorinput == 3) {
			smc.changeoutputmainmenucontrol(1);
		}
		startoff();
		
	}
	
	public void Paint_Upgrade(Graphics gfx){
		gfx.setColor(Color.WHITE); // upgrademenu
		gfx.drawImage(ImportImages.upgradebuilding, 0,0,800,600,null);
		gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 42));
		gfx.drawString("UPGRADE MENU", 250,65);
		gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 16));
		gfx.drawString("Money: ", 325, 100);
		gfx.drawString(playermoney, 400, 100);
		int length = commands.length;
		for (int i = 0; i < length; i++ ) { // shows the data of the player's upgrades
			if (cursorinput == i) {
				gfx.setColor(Color.CYAN);
			}	else {
				gfx.setColor(Color.WHITE);
			}
			// description
			
			gfx.drawString(commands[i], 50, 200 + i * 40);
			gfx.setColor(Color.WHITE);
			gfx.drawString("Level: " + missileupgrade + " Price: $1000", 220, 200);// status of the standard (called railgun in game) and price
			gfx.drawString("Lives: " + playerlives + " Price: $10000", 220, 280); // more lives
			gfx.drawString("No: " + multimissileupgrade + " Price: $1000", 220, 240);
			gfx.setColor(Color.RED);
			if ((checkmoney == 1) && (cursorinput == 0)) {
				gfx.drawString("   Not enough money to upgrade!   ", 420, 200);
			} else if ((lifemoney == 1) && (cursorinput == 2)) {
				gfx.drawString("   Not enough money ", 420, 280);
				 // declining upgrade as there are not enough upgrade
			} else if ((checkmoney == 2) && (cursorinput == 0)) {
				gfx.setColor(Color.GREEN); // allowing the upgrade
				gfx.drawString("   Upgrade purchased and installed  ", 420, 200);
			} else if ((lifemoney == 2) && (cursorinput == 2)) {
				gfx.setColor(Color.GREEN); // allowing the upgrade
				gfx.drawString("   Extra life brought", 420, 280);
			} else if (checkmoney == 3) {
				gfx.setColor(Color.YELLOW); // maximum number of upgrades reached
				gfx.drawString("   Maximum number of upgrades reached", 420, 200);
			} else if ((multiupgrademoney == 1) && (cursorinput == 1)) {
				gfx.drawString("   Not enough money to get missile", 420, 240);
			} else if ((multiupgrademoney == 2) && (cursorinput == 1)) {
				gfx.setColor(Color.GREEN); // allowing the upgrade
				gfx.drawString("   Upgrade purchased and installed  ", 420, 240);
			} else if ((multiupgrademoney == 3) && (cursorinput == 1)) {
				gfx.setColor(Color.YELLOW); // maximum number of upgrades reached
				gfx.drawString("   Maximum number of purchases made", 420, 240);
			}
		gfx.setColor(Color.WHITE);
		if (cursorinput == 0) {
			if (missileupgradenumber == 0) {
				gfx.drawString("Level 0: W12-23 RAILGUN: old but reliable, this gun ", 50, 500);
				gfx.drawString("         is used in many spaceship fighter companies", 50, 530);
				gfx.drawString("                                                    ", 50, 560);
			} else if (missileupgradenumber == 1) {
				gfx.drawString("Level 1: W12-24 RAILGUN: a sucessor to the W12-23,  ", 50, 500);
				gfx.drawString("         improved control system allows larger      ", 50, 530);
				gfx.drawString("         projectiles to be shot.                    ", 50, 560);
			} else if (missileupgradenumber == 2) {
				gfx.drawString("Level 2: W12-24.9 RAILGUN PROTO: Extension to the   ", 50, 500);
				gfx.drawString("         gun barrel allows larger projectile to be  ", 50, 530);
				gfx.drawString("         shot with greatly increased speed          ", 50, 560);
			} else if (missileupgradenumber == 3) {
				gfx.drawString("Level 3: W12-CLASSIFIED: Heavily modified version of", 50, 500);
				gfx.drawString("         the W12-24.9, the thicker barrel allows the", 50, 530);
				gfx.drawString("         use of battleship class projectiles        ", 50, 560);
			}
			
		} else if (cursorinput == 2) {
			gfx.drawString("Warp in another 'you' in an alternate universe :D       ", 50, 500);
		} else if (cursorinput == 1){
			gfx.drawString("Multishot 'Dellingr': 5x railgun shot  ", 50, 500);
			gfx.drawString("                      Effective for killing numerous enemies ", 50, 530);
			gfx.drawString("                      Press 'c' to use the 5 shot", 50, 560);
		} else if (cursorinput == 3) {
			gfx.drawString("Continue the campaign", 50, 500);
		}
		}
		
	}
	

}
