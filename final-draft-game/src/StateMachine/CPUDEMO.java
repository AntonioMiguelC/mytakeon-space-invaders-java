package StateMachine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import Centre.GameLayout;
import Centre.ImportImages;
import Model.AutonomousPlayerControl;
import Model.EnemyList;
import Model.Missile;
import Model.Shields;

public class CPUDEMO implements StateInterface {
	
	/*CPU Demo game screen*/

	public static Shields SH;
	public static StateMachineControl smc;
	public int missileupgrade; // to check if the missile is upgraded in the game
	public int multimissileupgrade;
	public static int score;
	public static int money;
	static String scorestring;
	public int missilescore;
	public static int hit;
	private static boolean setPause = false;
	private static boolean changedState = false;

	public static AutonomousPlayerControl playercontrol;
	public static EnemyList enemies;
	//private HashMap<String, ImportMusic> soundeffects;


	public CPUDEMO(StateMachineControl smc) {
		
		CPUDEMO.smc = smc;
		startoff();

	}

	
	public void startoff() { //Create instances of Visual Objects here
		
		playercontrol = new AutonomousPlayerControl(1); //Create Player
		enemies = new EnemyList();
		SH = new Shields();	
		missileupgrade = smc.missileupgrade;
		multimissileupgrade = smc.multimissileupgrade;
		scorestring = new Integer(score).toString();

		
	}
	public Rectangle getBounds(){
		return new Rectangle(75, 420, 45, 60);
	}
	

	/*Keeps track of what keys have been pressed in game*/
	public void keyPressed(int keycode) {
		playercontrol.keyPressed(keycode);
		if (keycode == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		} else if (keycode == KeyEvent.VK_BACK_SPACE){
			cpudemoexecution();
		} 
		
	}

	private void cpudemoexecution() {
		smc.changeoutputmainmenucontrol(0);
		
	}


	public void keyReleased(int keycode) {
		playercontrol.keyReleased(keycode);
		// TODO Auto-generated method stub
		
	}

	public void control() {
		
		if(changedState){
			changedState = false;
			score = 0;
			startoff();
		}
		
		playercontrol.control();
		playercontrol.autonomousControl();
		
		enemies.control();
		missileupgrade = smc.missileupgradecheck(missileupgrade); // check missile status
		multimissileupgrade = smc.multimissileupgradecheck(multimissileupgrade);
		missilescore = Missile.missilescore;
		playermissileupgrade(missileupgrade); // reupdate the upgraded missile to the player
		playermultimissilepurchase(multimissileupgrade); // update the special weapon status to the player
		scoreupdater();
		
		if (playercontrol.getLivesAsInt()== 0){
			changedState = true;
			smc.changeoutputmainmenucontrol(4);
		}
		
		if (enemies.isEmpty()){ //If all enemies have been eliminated
			//Survival Mode is non-stop.
			enemies.spawnSurvival();
		}
		
		for (int i = 0; i < enemies.getEnemies().size(); i++){//If enemies hit the bottom of the screen
			if (enemies.getEnemies().get(i).getYPos() + 35 >= GameLayout.GAME_HEIGHT){
				changedState = true;
				smc.changeoutputmainmenucontrol(4);
			}
		}
		
	}
	
	public static void changehit(int confirm) {
		
		if(enemies.isEmpty()){
			hit = 0;
		}
		else{
			hit = confirm; // input of hit
			scoreupdater(); // update score
		}
		
	}

	private static void scoreupdater() {
		
		if(enemies.isEmpty()){
			score += 0;
			money += 0;
		}
		else{
			score += hit; // add value of score
			money += hit * 100;
			scorestring = new Integer(score).toString(); // print data to screen
			hit = 0; //set to zero every time so that it doesnt accidentally keep incrementing 'score' once the enemy linkedlist is empty
		}
	
}
	private void playermissileupgrade(int upgrade) {
		playercontrol.playermissileupgrade(upgrade); // sending data to the player
		
	}
	
	private void playermultimissilepurchase(int multiupgrade) {
		playercontrol.playermultimissilepurchase(multiupgrade);
	}
	
	public static void usemultimissile(int used) {
		smc.multimissileused(used);
	}
	
	public void paint_CPU(Graphics gfx) { // display things here
		gfx.drawImage(ImportImages.galaxy, 0,0, 800,800,null);
		playercontrol.playerpictures(gfx);
		enemies.enemypictures(gfx);
		;
		SH.paint_Shield((Graphics2D) gfx);
		gfx.setColor(Color.MAGENTA);
		gfx.setFont(new Font("Lucida Console", Font.PLAIN, 32));
		gfx.drawString("PRESS BACKSPACE TO RETURN TO MAINMENU", 50,290);
		gfx.setFont(new Font("Lucida Console", Font.PLAIN, 15));
		gfx.setColor(Color.WHITE);
		gfx.drawString("SCORE: " + scorestring, 700, 20);
		gfx.drawString("LIVES: " + playercontrol.getLivesAsString(), 700, 35);
		if (playercontrol.getLivesAsInt() < 10) {
			gfx.setColor(Color.RED);
			gfx.drawString("LIVES: " + playercontrol.getLivesAsString(), 700, 35);
		}
		
		if (setPause){//Informs the user that the game is paused
			gfx.drawString("PAUSED!", 10, 20);
		}
		
	}
	
	public void mouseMoved(MouseEvent mouse) {
		/*
		 * Method not used
		 */
		
	}


	public void mouseClicked(MouseEvent mouse) {
		/*
		 * Method not used
		 */
		
	}
	
	public static void resumeGame(){
		setPause = false;
		enemies.resume();
		playercontrol.resume();
	}
	public static boolean getPauseStatus(){
		return setPause;
	}
	
	
	/*Private Methods*/
	
	private void pauseGame(){
		setPause = true;
		int keycode = 0;
		enemies.pause();
		playercontrol.pause();
		if (keycode == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
		
	}
}