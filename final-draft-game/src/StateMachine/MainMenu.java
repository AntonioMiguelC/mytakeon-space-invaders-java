package StateMachine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;

import Centre.GameLayout;
import Centre.ImportImages;
import Centre.ImportMusic;


public class MainMenu implements StateInterface {
	public StateMachineControl smc;
	String[] commands = {"Campaign", "Survival", "Help", "Demo", "Quit"};
	
	int cursorinput = 0;
	public static HashMap<String, ImportMusic> soundeffects;
	public static HashMap<String, ImportMusic> campsoundeffects;
	public static HashMap<String, ImportMusic> winsoundeffects;
	public static HashMap<String, ImportMusic> losesoundeffects;
	public static HashMap<String, ImportMusic> hitsoundeffects;
	public static HashMap<String, ImportMusic> deathsoundeffects;
	
	
	public static byte maincontrol = 0; // for main menu implementation
	
	public MainMenu(StateMachineControl smc) {
		this.smc = smc;
	}
	
	public void control() {
		// TODO Auto-generated method stub
		
	}


	public void paint_mainmenu(Graphics gfx) {
		int xaxispos = GameLayout.GAME_WIDTH / 2 + 90;
		int length = commands.length;
		gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 42));	
		gfx.drawImage(ImportImages.planetbackground, 0,0, 800,800,null); // input background image of planet here (free resource) http://www.spiralgraphics.biz/packs/planet/previews/Caldonia.jpg
		gfx.setColor(Color.WHITE);
		gfx.drawString("SPACE INVADERS", 230,40);
		for (int i = 0; i < length; i++ ) {
			if (cursorinput == i) {
				gfx.setColor(Color.CYAN);
			}	else {
				gfx.setColor(Color.WHITE);
			}
			gfx.drawString(commands[i], xaxispos, 370 + i * 40);
			}
		
		// TODO Auto-generated method stub
		
	}


	public void keyPressed(int keycode) {
		int down = KeyEvent.VK_DOWN; // right now the mainmenu only supports the up and down keys (will be changed to mouse)
		int up = KeyEvent.VK_UP;
		int enter = KeyEvent.VK_ENTER;
		int length = commands.length;
		if (keycode == down) {
			cursorinput = cursorinput + 1;
			if (cursorinput >= length) {
				cursorinput = 0;
			}
		} else if (keycode == up) {
			cursorinput = cursorinput - 1;
			if (cursorinput < 0) {
				cursorinput = length - 1;
			}
		} else if (keycode == enter) {
			mainmenuexecution();
		} else if (keycode == KeyEvent.VK_PAGE_UP) { // instantly go to win screen
			smc.changeoutputmainmenucontrol(3);			
		} else if (keycode == KeyEvent.VK_PAGE_DOWN) { // instantly go to lose screen
			smc.changeoutputmainmenucontrol(4);
		}
			
		
		
	}


	public void keyReleased(int keycode) {
		// TODO Auto-generated method stub
		
	}
	
	void mainmenuexecution() {
		
		soundeffects = new  HashMap<String, ImportMusic>();
		campsoundeffects = new  HashMap<String, ImportMusic>();
		winsoundeffects = new  HashMap<String, ImportMusic>();
		losesoundeffects = new  HashMap<String, ImportMusic>();
		hitsoundeffects = new  HashMap<String, ImportMusic>();
		deathsoundeffects = new  HashMap<String, ImportMusic>();
		
		if (cursorinput == 0) {
			smc.changeoutputmainmenucontrol(1);

				soundeffects = new  HashMap<String, ImportMusic>();
				soundeffects.put("Theme", new ImportMusic("/DragonForce.wav"));
				soundeffects.get("Theme").playwav();
				campsoundeffects.put("Theme", new ImportMusic("/DragonForce.wav"));
				campsoundeffects.get("Theme").playwav();

			
		} else if (cursorinput == 1) {

			smc.changeoutputmainmenucontrol(7);
			soundeffects.put("Sand", new ImportMusic("/Sandstorm.wav"));
			soundeffects.get("Sand").playwav();

		} else if (cursorinput == 2) {
			//soundeffects.get("Sand").stop();
			smc.changeoutputmainmenucontrol(2);
			
			//StateControl.levels.push(new Options(sc));
			
		} else if (cursorinput == 3) { // cpu demo mode
			smc.changeoutputmainmenucontrol(10);
		}
		
		else if (cursorinput == 4) {
			System.exit(0);	// quit the game
		}
		// TODO Auto-generated method stub
		
	}

	public void mouseMoved(MouseEvent mouse) {// implement the mouse control here
		int mousexpos = mouse.getX();// get xpos
		int mouseypos = mouse.getY();// get ypos
		// get area where the mouse cursor impacts the mainmenu controls
		if ((mousexpos > 480) && (mousexpos < 700) && (mouseypos > 330) && (mouseypos < 370) ) { 
			cursorinput = 0;
		} else if ((mousexpos > 480) && (mousexpos < 700) && (mouseypos > 370) && (mouseypos < 420)) {
			cursorinput = 1;
		} else if ((mousexpos > 480) && (mousexpos < 700) && (mouseypos > 420) && (mouseypos < 460)) {
			cursorinput = 2;
		} else if ((mousexpos > 480) && (mousexpos < 700) && (mouseypos > 460) && (mouseypos < 500)) {
			cursorinput = 3;
		} else if ((mousexpos > 480) && (mousexpos < 700) && (mouseypos > 500) && (mouseypos < 540)) {
			cursorinput = 4;
		} else {
			cursorinput = 5;
		}
			
	}
		//System.out.println(mousexpos + " " + mouseypos);
		// TODO Auto-generated method stub
		


	public void mouseClicked(MouseEvent mouse) {
		// input impact goes here
		int output = MouseEvent.BUTTON1;
		if (output == MouseEvent.BUTTON1) {
			mainmenuexecution();
		}
		// TODO Auto-generated method stub
		
	}

	public void startoff() {
		// TODO Auto-generated method stub
		
	}


}

