package StateMachine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;

import Centre.ImportImages;
import Centre.ImportMusic;

public class survivalpreparescreen  implements StateInterface{
	public StateMachineControl smc;
	String[] commands = {"SinglePlayer", "Co-Op", "Return"};
	int cursorinput = 0;
	public static HashMap<String, ImportMusic> soundeffects;
	public survivalpreparescreen(StateMachineControl smc) {
		
		this.smc = smc;
		// TODO Auto-generated constructor stub
	}

	public void startoff() {
		// TODO Auto-generated method stub
		
		
		
	}

	public void control() {
		// TODO Auto-generated method stub
		
	}

	public void keyPressed(int keycode) {
		int down = KeyEvent.VK_DOWN; // right now the mainmenu only supports the up and down keys (will be changed to mouse)
		int up = KeyEvent.VK_UP;
		int enter = KeyEvent.VK_ENTER;
		int length = commands.length;
		if (keycode == down) {
			cursorinput = cursorinput + 1;
			if (cursorinput >= length) {
				cursorinput = 0;
			}
		} else if (keycode == up) {
			cursorinput = cursorinput - 1;
			if (cursorinput < 0) {
				cursorinput = length - 1;
			}
		} else if (keycode == enter) {
			survialprepareexecution();
		} else if (keycode == KeyEvent.VK_PAGE_UP) { // instantly go to win screen
			smc.changeoutputmainmenucontrol(3);			
		} else if (keycode == KeyEvent.VK_PAGE_DOWN) { // instantly go to lose screen
			smc.changeoutputmainmenucontrol(4);
		}
		
		// TODO Auto-generated method stub
		
	}

	private void survialprepareexecution() {
		if (cursorinput == 0) {
			smc.changeoutputmainmenucontrol(8);
			
			//soundeffects.get("Theme").playwav();
			
		} else if (cursorinput == 1) {
			smc.changeoutputmainmenucontrol(9);
			
			
			//StateControl.levels.push(new Survival(sc)); // start survival mode
			
		} else if (cursorinput == 2) {
			smc.changeoutputmainmenucontrol(0);
			MainMenu.soundeffects.get("Sand").stop();
		}//StateControl.levels.push(new Options(sc));
			
		// TODO Auto-generated method stub
		
		// TODO Auto-generated method stub
		
	}

	public void keyReleased(int keycode) {
		// TODO Auto-generated method stub
		
	}

	public void mouseMoved(MouseEvent mouse) {
		int mousexpos = mouse.getX();// get xpos
		int mouseypos = mouse.getY();// get ypos
		// get area where the mouse cursor impacts the mainmenu controls
		if ((mousexpos > 450) && (mousexpos < 700) && (mouseypos > 360) && (mouseypos < 410) ) { 
			cursorinput = 0;
		} else if ((mousexpos > 450) && (mousexpos < 625) && (mouseypos > 410) && (mouseypos < 460)) {
			cursorinput = 1;
		} else if ((mousexpos > 450) && (mousexpos < 625) && (mouseypos > 460) && (mouseypos < 510)) {
			cursorinput = 2;
		} else {
			cursorinput = 3;
		}
		// TODO Auto-generated method stub
		
	}

	public void mouseClicked(MouseEvent mouse) {
		int output = MouseEvent.BUTTON1;
		if (output == MouseEvent.BUTTON1) {
			survialprepareexecution();
		}
		// TODO Auto-generated method stub
		
	}
	
	public void paint_survivalpreparescreen(Graphics gfx) { // display things here

		//playercontrol.playerpictures(gfx);
		//enemies.enemypictures(gfx);
		
		//SH.paint_Shield((Graphics2D) gfx);
		gfx.drawImage(ImportImages.survival, -100,-100, 1400,800,null);
		int length = commands.length;
		int xaxispos = 450;
		gfx.setFont(new Font("Lucida Console", Font.PLAIN, 35));
		gfx.setColor(Color.WHITE);
		gfx.drawString("SURVIVAL MODE", 250, 35);
		for (int i = 0; i < length; i++ ) {
			if (cursorinput == i) {
				gfx.setColor(Color.RED);
			}	else {
				gfx.setColor(Color.WHITE);
			}
			gfx.drawString(commands[i], xaxispos, 400 + i * 50);
		}
			
		

		
	}

}
