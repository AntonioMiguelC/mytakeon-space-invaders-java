package StateMachine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import Centre.ImportImages;
import Model.CoinList;
import Model.EnemyList;
import Model.PlayerControl;

public class BonusRound implements StateInterface {
	
	public static StateMachineControl smc;
	public static PlayerControl playercontrol;
	public static CoinList coins;
	private static boolean setPause = false;
	public static int hit;
	public static int score;
	public static int money;
	static String scorestring;
	
	
	public BonusRound(StateMachineControl smc) {
		// TODO Auto-generated constructor stub
		this.smc = smc;
		startoff();

	}

	public void startoff() {
		// TODO Auto-generated method stub
		playercontrol = new PlayerControl(1);
		coins = new CoinList();
	}

	public void control() {
		// TODO Auto-generated method stub
		playercontrol.control();
		coins.control();
		
		if (coins.isEmpty()){ //If all coins have been eliminated
			smc.changeoutputmainmenucontrol(3);
		}
		
	}
	
	public static void changehit(int confirm) {
		
		if(coins.isEmpty()){
			hit = 0;
		}
		else{
			hit = confirm; // input of hit
			scoreupdater(); // update score
		}
	}
	
	private static void scoreupdater() {
		
		if(coins.isEmpty()){
			score += 0;
			money += 0;
		}
		else{
			score += hit; // add value of score
			money += hit * 100;
			scorestring = new Integer(score).toString(); // print data to screen
			hit = 0; //set to zero every time so that it doesnt accidentally keep incrementing 'score' once the enemy linkedlist is empty
		}
	
}
	
	public Rectangle getBounds(){
		return new Rectangle(75, 420, 45, 60);
	}
	
	public void keyPressed(int keycode) {
		// TODO Auto-generated method stub
		playercontrol.keyPressed(keycode);
		if (keycode == KeyEvent.VK_P){
			if (!setPause){
				pauseGame();
				System.out.println("Pause");//for debugging
			}else resumeGame();
		    }
	   }

	public void keyReleased(int keycode) {
		// TODO Auto-generated method stub
		playercontrol.keyReleased(keycode);
		
	}

	public void mouseMoved(MouseEvent mouse) {
		// TODO Auto-generated method stub
		
	}

	public void mouseClicked(MouseEvent mouse) {
		// TODO Auto-generated method stub
		
	}
	
	public void paint_bonusround(Graphics gfx) {
		gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 42));	
		gfx.drawImage(ImportImages.planetbackground, 0,0, 800,800,null); // input background image of planet here (free resource) http://www.spiralgraphics.biz/packs/planet/previews/Caldonia.jpg
		playercontrol.playerpictures(gfx);
		coins.enemypictures(gfx);
		gfx.setColor(Color.GREEN);
		gfx.drawString("BONUS ROUND GOES HERE", 200,300);
	}

	private void pauseGame(){
		setPause = true;
		int keycode = 0;
		coins.pause();
		playercontrol.pause();
		if (keycode == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
		
	}
	
	public static void resumeGame(){
		setPause = false;
		coins.resume();
		playercontrol.resume();
	}
	public static boolean getPauseStatus(){
		return setPause;
	}
}
