package StateMachine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

import Centre.ImportImages;

public class Losescreen implements StateInterface {

	StateMachineControl smc;
	public Losescreen(StateMachineControl smc) {
		this.smc = smc;
		// TODO Auto-generated constructor stub
	}

	public void startoff() {
		// TODO Auto-generated method stub
		
	}

	public void control() {
		// TODO Auto-generated method stub
		
	}

	public void keyPressed(int keycode) {
		if (keycode > 0) {
			losescreenexecution(); // any key can be pressed
		}
		// TODO Auto-generated method stub
		
	}

	public void keyReleased(int keycode) {
		// TODO Auto-generated method stub
		
	}

	public void mouseMoved(MouseEvent mouse) {
		// TODO Auto-generated method stub
		
	}

	public void mouseClicked(MouseEvent mouse) {
		int output = MouseEvent.BUTTON1;
		if (output == MouseEvent.BUTTON1) { // mouse clicked
			losescreenexecution();
		}
		// TODO Auto-generated method stub
		
	}

	
	private void losescreenexecution() {
		
		smc.changeoutputmainmenucontrol(0); // return to main menu
		// TODO Auto-generated method stub
		
	}

	public void paint_losescreen(Graphics gfx) {
		gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 42));	
		gfx.drawImage(ImportImages.defeat, 0,0, 800,600,null); // input background image of planet here (free resource) http://www.spiralgraphics.biz/packs/planet/previews/Caldonia.jpg
		gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 24));
		gfx.setColor(Color.RED);
		gfx.drawString("Press any key to Continue" , 250, 450);
		if (smc.previousstate == 1) {
			gfx.drawString("Your Score for Campaign: " + Campagin1_1.score, 250, 480);
		} else if (smc.previousstate == 8) {
			gfx.drawString("Your Score for Solo Survival: " + SurvivalSolo.score, 250, 480);
		} else if (smc.previousstate == 9) {
			gfx.drawString("Your Score for Co-op Survival: " + SurvivalCo_op.score, 250, 480);
		}
	}
}

