package StateMachine;

import java.awt.event.MouseEvent;

public interface StateInterface {
	
	
	
	public abstract void startoff();

	public abstract void control();

	public abstract void keyPressed(int keycode);

	public abstract void keyReleased(int keycode);

	public abstract void mouseMoved(MouseEvent mouse);

	public abstract void mouseClicked(MouseEvent mouse);

}
