package StateMachine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import Centre.GameLayout;
import Centre.ImportImages;

public class Options implements StateInterface{

	public StateMachineControl smc;
	String[] commands = {"RETURN"};
	
	int cursorinput = 0;
	
	public static byte optionscontrol = 0; // for main menu implementation
	
	public Options(StateMachineControl smc) {
		this.smc = smc;
	}
	
	public void startoff() {
		// TODO Auto-generated method stub
		
	}

	public void control() {
		// TODO Auto-generated method stub
		
	}
	
	public void paint_Options(Graphics gfx) { // options (now help) -- this part will show the player and the screens
		int xaxispos = GameLayout.GAME_WIDTH / 2 + 90;
		int length = commands.length;
		gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 42));	
		gfx.drawImage(ImportImages.controls, 100,200, 600,400,null); // input background image of planet here (free resource) http://www.spiralgraphics.biz/packs/planet/previews/Caldonia.jpg
		gfx.setColor(Color.WHITE);
		gfx.drawString("HELP", 350,65);
		gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 18));
		gfx.drawImage(ImportImages.player, 100, 125, 60, 67, null);
		gfx.setColor(Color.BLUE);
		gfx.drawString("Player(You)", 75, 210);
		gfx.drawString("Special weapon can only be used in Campaign", 30, 275);
		gfx.setColor(Color.GREEN);
		gfx.drawImage(ImportImages.secondplayer11,360, 125, 60, 67, null);
		gfx.drawString("Player2", 350, 210);
		gfx.setColor(Color.RED);
		gfx.drawImage(ImportImages.enemy, 630, 155, 30, 30, null);
		gfx.drawString("Aliens(Enemy)", 580, 210);
		gfx.drawString("(You have to kill them!)", 525, 240);
		gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 42));	
		for (int i = 0; i < length; i++ ) {
			if (cursorinput == i) {
				gfx.setColor(Color.CYAN);
			}	else {
				gfx.setColor(Color.WHITE);
			}
			gfx.drawString(commands[i], xaxispos, 550 + i * 40);
			}
		
	}

	public void keyPressed(int keycode) {
		int down = KeyEvent.VK_DOWN; // right now the mainmenu only supports the up and down keys (will be changed to mouse)
		int up = KeyEvent.VK_UP;
		int enter = KeyEvent.VK_ENTER;
		int length = commands.length;
		if (keycode == down) {
			cursorinput = cursorinput + 1;
			if (cursorinput >= length) {
				cursorinput = 0;
			}
		} else if (keycode == up) {
			cursorinput = cursorinput - 1;
			if (cursorinput < 0) {
				cursorinput = length - 1;
			}
		} else if (keycode == enter) {
			optionsexecution();
		} else if (keycode == KeyEvent.VK_PAGE_UP) { // instantly go to win screen
			smc.changeoutputmainmenucontrol(3);			
		} else if (keycode == KeyEvent.VK_PAGE_DOWN) { // instantly go to lose screen
			smc.changeoutputmainmenucontrol(4);
		}
		
	}

	private void optionsexecution() {
		if (cursorinput == 0) {
			smc.changeoutputmainmenucontrol(0);
			//System.out.println(maincontrol);
		} 
		
	}

	public void keyReleased(int keycode) {
		// TODO Auto-generated method stub
		
	}

	public void mouseMoved(MouseEvent mouse) {
		int mousexpos = mouse.getX(); // xaxis mouse
		int mouseypos = mouse.getY(); // yaxis mouse
		// area of mouse cursor effecting the options menu
		if ((mousexpos > 480) && (mousexpos < 700) && (mouseypos > 500) && (mouseypos < 550) ) {
			cursorinput = 0;
		} else {
			cursorinput = 1;
		}
		
	}

	public void mouseClicked(MouseEvent mouse) {
		// the execution button for mouse
		int output = MouseEvent.BUTTON1;
		if (output == MouseEvent.BUTTON1) {
			optionsexecution();
		}
		// TODO Auto-generated method stub
		
	}

}
