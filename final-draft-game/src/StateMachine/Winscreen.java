package StateMachine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

import Centre.ImportImages;
import Centre.ImportMusic;

public class Winscreen implements StateInterface {

	StateMachineControl smc;

	public Winscreen(StateMachineControl smc) {
		this.smc = smc;
		// TODO Auto-generated constructor stub
	}

	public void startoff() {
		// TODO Auto-generated method stub
		
	}

	public void control() {
		
		
	}

	public void keyPressed(int keycode) {
		if (keycode > 0) {
			winscreenexecution(); // any key to return to main menu
		}
		// TODO Auto-generated method stub
		
	}

	private void winscreenexecution() {
		MainMenu.winsoundeffects.get("MLG").stop();
		smc.changeoutputmainmenucontrol(0); // return to main menu
		// TODO Auto-generated method stub
		
	}

	public void keyReleased(int keycode) {
		// TODO Auto-generated method stub
		
	}

	public void mouseMoved(MouseEvent mouse) {
		// TODO Auto-generated method stub
		
	}

	public void mouseClicked(MouseEvent mouse) {
		int output = MouseEvent.BUTTON1;
		if (output == MouseEvent.BUTTON1) { // mouse click can return to main menu
			winscreenexecution();
		}
		// TODO Auto-generated method stub
		
	}
	
	public void paint_winscreen(Graphics gfx) {
		gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 30));	
		gfx.drawImage(ImportImages.victory, 0,0, 800,600,null); // input background image of planet here (free resource) http://www.spiralgraphics.biz/packs/planet/previews/Caldonia.jpg
		gfx.setColor(Color.GREEN);
		//gfx.drawString("YOU WIN", 200,300);
		//gfx.setFont(new Font ("Lucida Console", Font.PLAIN, 24));
		//gfx.setColor(Color.WHITE);
		gfx.drawString("Press any key to Continue" , 50, 550);
		if (smc.previousstate == 1) {
			gfx.drawString("Your Score for Campaign: " + Campagin1_1.score, 50, 580);
		} else if (smc.previousstate == 8) {
			gfx.drawString("Your Score for Solo Survival: " + SurvivalSolo.score, 50, 580);
		} else if (smc.previousstate == 9) {
			gfx.drawString("Your Score for Co-op Survival: " + SurvivalCo_op.score, 50, 580);
		}
	}

}
