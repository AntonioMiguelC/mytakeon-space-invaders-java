package StateMachine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;

import Centre.GameLayout;
import Centre.ImportImages;
import Centre.ImportMusic;
import Model.EnemyList;
import Model.Missile;
import Model.PlayerControl;
import Model.Shields;

public class Campagin1_1 implements StateInterface{
	
	/*Main Campaign class*/
	
	public static Shields SH;
	public static StateMachineControl smc;
	public int missileupgrade; // to check if the missile is upgraded in the game
	public int multimissileupgrade;
	public static int score;
	public static int money;
	static String scorestring;
	public int missilescore;
	public static int hit;
	private static boolean setPause = false;

	public static boolean changedState = false;

	public static PlayerControl playercontrol;
	public static HashMap<String, ImportMusic> soundeffects;
	public static HashMap<String, ImportMusic> campsoundeffects;
	public static HashMap<String, ImportMusic> winsoundeffects;
	public static HashMap<String, ImportMusic> losesoundeffects;
	public static HashMap<String, ImportMusic> deathsoundeffects;
	
	public static EnemyList enemies;
	private int spawnCount = 0;


	public Campagin1_1(StateMachineControl smc) {
		
		Campagin1_1.smc = smc;
		startoff();
		

	}

	
	public void startoff() { //Create instances of Visual Objects here
		
		playercontrol = new PlayerControl(1); // make player in the campaign
		enemies = new EnemyList(); //instantiate a new enemy
		SH = new Shields();	//instantiates the shields
		
		//enables upgrades
		missileupgrade = smc.missileupgrade; 
		multimissileupgrade = smc.multimissileupgrade;
		
		scorestring = new Integer(score).toString();
		
		pauseGame();
		
	}
	
	public Rectangle getBounds(){
		return new Rectangle(75, 420, 45, 60);
	}
	

	/*Keeps track of what keys have been pressed in game*/
	public void keyPressed(int keycode) {
		playercontrol.keyPressed(keycode);
		System.out.println(keycode);
		if (keycode == KeyEvent.VK_PAGE_UP) { // instantly go to win screen
			smc.changeoutputmainmenucontrol(3);			
		} else if (keycode == KeyEvent.VK_PAGE_DOWN) { // instantly go to lose screen
			smc.changeoutputmainmenucontrol(4);
		} else if (keycode == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		} else if (keycode == KeyEvent.VK_P){
			if (!setPause){
				pauseGame();
				System.out.println("Pause");//for debugging
			}else resumeGame();
		} else if (keycode == KeyEvent.VK_U) {
			if (setPause == true){
				resumeGame(); //prevents a previous bug where enemies dont move when resuming game
			}
			pauseGame();
			smc.changeoutputmainmenucontrol(5);
		}
		// TODO Auto-generated method stub
		
	}

	public void keyReleased(int keycode) {
		playercontrol.keyReleased(keycode);
		// TODO Auto-generated method stub
		
	}

	public void control() {
		
		/*If replaying a new game*/
		if(changedState){
			changedState = false;
			score = 0;
			startoff();
		}
		///////////////////////////
		
		playercontrol.control();
		enemies.control();
		
		missileupgrade = smc.missileupgradecheck(missileupgrade); // check missile status
		multimissileupgrade = smc.multimissileupgradecheck(multimissileupgrade);
		
		missilescore = Missile.missilescore;
		playermissileupgrade(missileupgrade); // reupdate the upgraded missile to the player
		playermultimissilepurchase(multimissileupgrade); // update the special weapon status to the player
		
		scoreupdater();
		
		if (playercontrol.getLivesAsInt()== 0){
			changedState = true;

			MainMenu.losesoundeffects.put("Death", new ImportMusic("/Death.wav"));
			MainMenu.losesoundeffects.get("Death").playwav();
			MainMenu.campsoundeffects.get("Theme").stop();
			MainMenu.soundeffects.get("Theme").stop();
			MainMenu.losesoundeffects.put("Lose", new ImportMusic("/Lose.wav"));
			MainMenu.losesoundeffects.get("Lose").playwav();
			smc.changeoutputmainmenucontrol(4);
		}
		
		if (enemies.isEmpty()){ //If all enemies have been eliminated
			spawnCount++;
			
			if(spawnCount == 1){
				
				enemies.spawnGold();
			}
			else if (spawnCount == 2){
				
				enemies.spawnBlue();
				
			}
			else if(spawnCount == 3){
				enemies.spawnRand();
			}
			else{
				changedState = true;
				MainMenu.campsoundeffects.get("Theme").stop();
				MainMenu.winsoundeffects.put("MLG", new ImportMusic("/MLG.wav"));
				MainMenu.winsoundeffects.get("MLG").playwav();
				smc.changeoutputmainmenucontrol(3);
			}
		}
		
		for (int i = 0; i < enemies.getEnemies().size(); i++){//If enemies hit the bottom of the screen
			if (enemies.getEnemies().get(i).getYPos() + 35 >= GameLayout.GAME_HEIGHT){
				changedState = true;
				MainMenu.losesoundeffects.put("Death", new ImportMusic("/Death.wav"));
				MainMenu.losesoundeffects.get("Death").playwav();
				MainMenu.campsoundeffects.get("Theme").stop();
				MainMenu.losesoundeffects.put("Lose", new ImportMusic("/Lose.wav"));
				MainMenu.losesoundeffects.get("Lose").playwav();
				smc.changeoutputmainmenucontrol(4); //GAME OVER
			}
		}

	}
	
	public static void changehit(int confirm) {
		
		if(enemies.isEmpty()){
			hit = 0;
		}
		else{
			hit = confirm; // input of hit
			scoreupdater(); // update score
		}
		
	}

	private static void scoreupdater() {
		
		if(enemies.isEmpty()){
			score += 0;
			money += 0;
		}
		else{
			score += hit; // add value of score
			money += hit * 100;
			scorestring = new Integer(score).toString(); // print data to screen
			hit = 0; //set to zero every time so that it doesnt accidentally keep incrementing 'score' once the enemy linkedlist is empty
		}
	
}
	private void playermissileupgrade(int upgrade) {
		playercontrol.playermissileupgrade(upgrade); // sending data to the player
		
	}
	
	private void playermultimissilepurchase(int multiupgrade) {
		playercontrol.playermultimissilepurchase(multiupgrade);
	}
	
	public static void usemultimissile(int used) {
		smc.multimissileused(used);
	}
	
	public void paint_camp(Graphics gfx) { // display things here
		gfx.drawImage(ImportImages.galaxy, 0,0, 800,800,null);
		playercontrol.playerpictures(gfx);
		enemies.enemypictures(gfx);
		SH.paint_Shield((Graphics2D) gfx);
		
		gfx.setFont(new Font("Lucida Console", Font.PLAIN, 15));
		gfx.setColor(Color.WHITE);
		gfx.drawString("SCORE: " + scorestring, 700, 20); // standard display
		gfx.drawString("LIVES: " + playercontrol.getLivesAsString(), 700, 35); // lives of the player

		if (playercontrol.getLivesAsInt() < 10) { // WARN THE PLAYER OF LOW LIVES
			gfx.setColor(Color.RED); // change the colour
			gfx.drawString("LIVES: " + playercontrol.getLivesAsString(), 700, 35);
		}
		
		if (playercontrol.multimissilestock > 25) { // no warning
			gfx.setColor(Color.WHITE);
			gfx.drawImage(ImportImages.multishot, 700, 40, 25,25,null); // lot of special weapons to shoot
			gfx.drawString(" x " + playercontrol.multimissilestock, 730, 57);
		} else if (playercontrol.multimissilestock <= 25 && playercontrol.multimissilestock > 0) { // low special weapons, warn the player
			gfx.setColor(Color.YELLOW); // change the colour of the displayed string
			gfx.drawImage(ImportImages.multishotwarning, 700, 40, 25,25,null);
			gfx.drawString(" x " + playercontrol.multimissilestock, 730, 57);
		} else if (playercontrol.multimissilestock == 0) { // player ran out of the special weapons
			gfx.setColor(Color.RED); // change the colour to red to tell the player that there are no more special weapons.
			gfx.drawImage(ImportImages.multishotnone, 700, 40, 25,25,null);
			gfx.drawString(" x " + playercontrol.multimissilestock, 730, 57);
		}
		gfx.setColor(Color.WHITE);
		
		if (setPause){//Informs the user that the game is paused
			gfx.drawString("PAUSED!", GameLayout.GAME_WIDTH / 2 - 30, GameLayout.GAME_HEIGHT / 2);
			gfx.drawString("Press 'P' to resume.", GameLayout.GAME_WIDTH / 2 - 30, 15 + GameLayout.GAME_HEIGHT / 2);
		}
		
	}
	
	public void mouseMoved(MouseEvent mouse) {
		// TODO Auto-generated method stub
		
	}


	public void mouseClicked(MouseEvent mouse) {
		// TODO Auto-generated method stub\
		
	}
	
	
	/*Private Methods*/
	private void pauseGame(){
		setPause = true;
		int keycode = 0;
		enemies.pause();
		playercontrol.pause();
		if (keycode == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
		
	}
	
	public static void resumeGame(){
		setPause = false;
		enemies.resume();
		playercontrol.resume();
	}
	
	public static boolean getPauseStatus(){
		return setPause;
	}
	


}
