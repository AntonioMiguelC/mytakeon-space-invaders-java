package StateMachine;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class StateMachineControl {
	/*
	 * This class defines the different states
	 * and has the method to control state transitions
	 */
	
	public ArrayList<StateInterface> states;
	
	public static int statecurrent = 0;
	
	private int MAINMENU = 0; // initialise the state numbers here
	private int CAMPAGIN1_1 = 1;
	private int OPTIONS = 2;
	private int WINSCREEN = 3;
	private int LOSESCREEN = 4;
	private int UPGRADEMENU = 5;
	private int BONUSROUND =6;
	private int SURVIVALPREPARESCREEN = 7;
	private int SURVIVALSOLO = 8;
	private int SURVIVALCO_OP = 9;
	private int CPUDEMO = 10;
	
	public static int outputmainmenucontrol = 0; // impacting state control
	public int money = 1000;
	public int missileupgrade = 0;
	public int multimissileupgrade = 0;
	public int score = 0;
	public int previousstate;
	
	public MainMenu mainmenu; // initiate the main menu here
	public Campagin1_1 camp1;
	public Options option;
	public Winscreen winscreen;
	public Losescreen losescreen;
	public UpgradeMenu upgrademenu;
	public BonusRound bonusround;
	public survivalpreparescreen survivalpreparescreen;
	public SurvivalSolo survivalsolo;
	public SurvivalCo_op survivalco_op;
	public CPUDEMO cpudemo;
	
	public StateMachineControl() {
		states = new ArrayList<StateInterface>(); // constructor
		statemachinecontroller();
		mainmenu = new MainMenu(this);
		states.add(mainmenu);
		camp1 = new Campagin1_1(this);
		states.add(camp1);
		option = new Options(this);
		states.add(option);
		winscreen = new Winscreen(this);
		states.add(winscreen);
		losescreen = new Losescreen(this);
		states.add(losescreen);
		upgrademenu = new UpgradeMenu(this);
		states.add(upgrademenu);
		bonusround = new BonusRound(this);
		states.add(bonusround);
		survivalpreparescreen = new survivalpreparescreen(this);
		states.add(survivalpreparescreen);
		survivalsolo = new SurvivalSolo(this);
		states.add(survivalsolo);
		survivalco_op = new SurvivalCo_op(this);
		states.add(survivalco_op);
		cpudemo = new CPUDEMO(this);
		states.add(cpudemo);
	}
	public void statemachinecontroller() {
		

		
		if (outputmainmenucontrol == 0) {// state mainmenu 
			NewState(MAINMENU);

		}
		else if (outputmainmenucontrol == 1) { // state campagin1-1
			NewState(CAMPAGIN1_1);
		}
		else if (outputmainmenucontrol == 2) {
			NewState(OPTIONS);
		}
		else if (outputmainmenucontrol == 3) {
			NewState(WINSCREEN);
		}
		
		else if (outputmainmenucontrol == 4) {
			NewState(LOSESCREEN);
		}
		
		else if (outputmainmenucontrol == 5) {
			NewState(UPGRADEMENU);
		}
		
		else if (outputmainmenucontrol == 6){
			NewState(BONUSROUND);
		}
		else if (outputmainmenucontrol == 7) {
			NewState(SURVIVALPREPARESCREEN);
		}
		else if (outputmainmenucontrol == 8) {
			NewState(SURVIVALSOLO);
		}
		else if (outputmainmenucontrol == 9) {
			NewState(SURVIVALCO_OP);
		}
		else if (outputmainmenucontrol == 10) {
			NewState(CPUDEMO);
		}
		//System.out.println(MainMenu.maincontrol);
	}

	void NewState(int state) {
		state = statecurrent;
	}
	
	public void control() {
		states.get(statecurrent).control();
	}
	

	public void changeoutputmainmenucontrol(int value) {
		outputmainmenucontrol = value;
		datachecker();
	}
	
	public int missileupgradecheck(int check) { // double check if the upgrade is applied to the player
		if (check != missileupgrade) {
			check = missileupgrade;
			return check; // return fixed value;
			}
		return check;
	}
	
	public void missileupgrade(int upgrade) { // upgrade missile here
		if (upgrade == 1) {
			missileupgrade = missileupgrade + 1;
		}
	}
	
	public void multimissileupgrade(int multiupgrade) {
		if (multiupgrade == 1) {
			multimissileupgrade = multimissileupgrade + 1;
		}
	}
	
	public void lifeadd (int life) { // add life to the system
		if (life == 1) {
			Campagin1_1.playercontrol.updatelives(1);
		}
	}

	public void keyPressed(int keycode) { 
		datachecker();
		states.get(statecurrent).keyPressed(keycode);
	}

	public void keyReleased(int keycode) {
		states.get(statecurrent).keyReleased(keycode);	
	}

	public void datachecker() {
		previousstate = statecurrent;
		if(outputmainmenucontrol != statecurrent) {
			statecurrent = outputmainmenucontrol;
		}
		
	}
	public void mouseMoved(MouseEvent mouse) {
		states.get(statecurrent).mouseMoved(mouse);	
	}
	
	public void mouseClicked(MouseEvent mouse) {
		datachecker();
		states.get(statecurrent).mouseClicked(mouse);	
	}
	public int multimissileupgradecheck(int checker) { // upgrade checker for spaceship (instead of standard one)
		if (checker != multimissileupgrade) {
			checker = multimissileupgrade;
			return checker; // return fixed value;
			}
		return checker;
	}
	public void multimissileused(int used) {
		multimissileupgrade = multimissileupgrade - used;
		
	}

	
}
