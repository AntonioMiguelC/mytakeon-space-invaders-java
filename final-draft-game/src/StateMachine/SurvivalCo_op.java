package StateMachine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;

import Centre.ImportImages;
import Centre.ImportMusic;
import Model.EnemyList;
import Model.Missile;
import Model.PlayerControl;
import Model.Shields;

public class SurvivalCo_op implements StateInterface {

	//public int totalives;
		public static Shields SH;
		public static StateMachineControl smc;
		public int missileupgrade; // to check if the missile is upgraded in the game
		public int multimissileupgrade;
		public static int score;
		public static int money;
		static String scorestring;
		public int missilescore;
		public static int hit;
		private static boolean setPause = false;

		private static boolean changedState = false;

		public static HashMap<String, ImportMusic> soundeffects;
		public static HashMap<String, ImportMusic> campsoundeffects;
		public static HashMap<String, ImportMusic> winsoundeffects;
		public static HashMap<String, ImportMusic> losesoundeffects;
		public static HashMap<String, ImportMusic> deathsoundeffects;

		public static PlayerControl playercontrol;
		public static PlayerControl playercontrol2;
		public static EnemyList enemies;
		


		public SurvivalCo_op(StateMachineControl smc) {
			
			this.smc = smc;
			startoff();

		}

		
		public void startoff() { //Create instances of Visual Objects here
			
			playercontrol = new PlayerControl(1); // make player in the campaign
			playercontrol2 = new PlayerControl(2);
			enemies = new EnemyList();
			SH = new Shields();	
			missileupgrade = smc.missileupgrade;
			multimissileupgrade = smc.multimissileupgrade;
			scorestring = new Integer(score).toString();
			
		}
		public Rectangle getBounds(){
			return new Rectangle(75, 420, 45, 60);
		}
		

		/*Keeps track of what keys have been pressed in game*/
		public void keyPressed(int keycode) {
			
			playercontrol.keyPressed(keycode);
			playercontrol2.keyPressed(keycode);
			if (keycode == KeyEvent.VK_PAGE_UP) { // instantly go to win screen DEBUGGING
				smc.changeoutputmainmenucontrol(3);			
			} else if (keycode == KeyEvent.VK_PAGE_DOWN) { // instantly go to lose screen
				smc.changeoutputmainmenucontrol(4);
			} else if (keycode == KeyEvent.VK_ESCAPE) {
				System.exit(0);
			} else if (keycode == KeyEvent.VK_P){
				if (!setPause){
					pauseGame();
					System.out.println("Pause");//for debugging
				}else resumeGame();
				
			}
		}

		public void keyReleased(int keycode) {
			playercontrol.keyReleased(keycode);
			playercontrol2.keyReleased(keycode);
			// TODO Auto-generated method stub
			
		}

		public void control() {
			
			if(changedState){
				changedState = false;
				score = 0;
				startoff();
			}
			
			playercontrol.control();
			playercontrol2.control();
			enemies.control();
			missilescore = Missile.missilescore;
			scoreupdater();
			
			if (playercontrol.getLivesAsInt()== 0 && playercontrol2.getLivesAsInt()== 0){
				
				changedState = true;

				MainMenu.losesoundeffects.put("Death", new ImportMusic("/Death.wav"));
				MainMenu.losesoundeffects.get("Death").playwav();
				MainMenu.soundeffects.get("Sand").stop();
				MainMenu.losesoundeffects.put("Lose", new ImportMusic("/Lose.wav"));
				MainMenu.losesoundeffects.get("Lose").playwav();
				smc.changeoutputmainmenucontrol(4);
				
			}
			
			/*The game will stop only if both player 1 and 2 die else only the one with
			 * zero lives will be removed
			 */
			else if (playercontrol.getLivesAsInt()== 0 || playercontrol2.getLivesAsInt()== 0){
				
				if(playercontrol.getLivesAsInt()== 0){
					playercontrol.remove();
				}
				else{
					playercontrol2.remove();
				}
				
				
			}
			
			if (enemies.isEmpty()){ //If all enemies have been eliminated
				
				enemies.spawnSurvival();
			}
		
			if (!setPause){ 
			}
		
			
		}
		
		public static void changehit(int confirm) {
			
			if(enemies.isEmpty()){
				hit = 0;
			}
			else{
				hit = confirm; // input of hit
				scoreupdater(); // update score
			}
			
		}

		private static void scoreupdater() {
			
			if(enemies.isEmpty()){
				score += 0;
				money += 0;
			}
			else{
				score += hit; // add value of score
				money += hit * 100;
				scorestring = new Integer(score).toString(); // print data to screen
				hit = 0; //set to zero every time so that it doesnt accidentally keep incrementing 'score' once the enemy linkedlist is empty
			}
		
	}
		
		
		public void paint_survivalcoop(Graphics gfx) { // display things here
			gfx.drawImage(ImportImages.galaxy, 0,0, 800,800,null);
			playercontrol.playerpictures(gfx);
			playercontrol2.playerpictures(gfx);
			enemies.enemypictures(gfx);
			;
			SH.paint_Shield((Graphics2D) gfx);
			
			gfx.setFont(new Font("Lucida Console", Font.PLAIN, 15));
			gfx.setColor(Color.WHITE);
			gfx.drawString("SCORE: " + scorestring, 700, 20);
			gfx.drawString("PLAYER 1: " + playercontrol.getLivesAsString(), 650, 35); // display data normally (as white font) if there are enough lives
			gfx.drawString("PLAYER 2: " + playercontrol2.getLivesAsString(), 650, 50);
			if (playercontrol.getLivesAsInt() < 10) {  // if the player1 lives are low
				gfx.setColor(Color.RED); // change font colour to warn player 1
				gfx.drawString("PLAYER 1: " + playercontrol.getLivesAsString(), 650, 35);
			}
			if (playercontrol2.getLivesAsInt() < 10) { // if the player2 lives are low
				gfx.setColor(Color.RED); // change font colour to warn player 2
				gfx.drawString("PLAYER 2: " + playercontrol2.getLivesAsString(), 650, 50);
			}
			if (setPause){//Informs the user that the game is paused
				gfx.drawString("PAUSED!", 10, 20);
			}
			
		}

		public void mouseMoved(MouseEvent mouse) {
			// TODO Auto-generated method stub
			
		}


		public void mouseClicked(MouseEvent mouse) {
			// TODO Auto-generated method stub\
			
		}
		
		public static boolean getPauseStatus(){
			return setPause;
		}
		
		
		/*Private Methods*/
		
		private void pauseGame(){
			setPause = true;
			int keycode = 0;
			enemies.pause();
			playercontrol.pause();
			playercontrol2.pause();
			if (keycode == KeyEvent.VK_ESCAPE) {
				System.exit(0);
			}
			
		}
		
		public static void resumeGame(){
			setPause = false;
			enemies.resume();
			playercontrol.resume();
			playercontrol2.resume();
		}

}
