package Model;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.LinkedList;

import StateMachine.CPUDEMO;
import StateMachine.Campagin1_1;
import StateMachine.StateMachineControl;
import StateMachine.SurvivalCo_op;
import StateMachine.SurvivalSolo;
import Centre.GameLayout;
import Centre.ImportImages;
import Centre.ImportMusic;
public class Missile extends Rectangle{
	
	/*
	 *Player's missile class. 
	 */
	private static final long serialVersionUID = 1L;
	private int missileymovement;
	public int ypos;
	public int xpos;
	private boolean isPaused = false;
	private static int missileupgrade;
	public static int missilescore = Campagin1_1.score;
	private HashMap<String, ImportMusic> soundeffects;
	private LinkedList<EnemyControl> Campenemies = Campagin1_1.enemies.getEnemies();

	private LinkedList<EnemyControl> Survienemies = SurvivalCo_op.enemies.getEnemies();
	private LinkedList<EnemyControl> SurvienemiesSolo = SurvivalSolo.enemies.getEnemies();
	private LinkedList<EnemyControl> Demoenemies = CPUDEMO.enemies.getEnemies();
	
	public Missile(int xpos, int ypos, int missileymovement) { // input data and get the movement speed of the missile
		this.missileymovement = missileymovement;
		this.ypos = ypos;
		this.xpos = xpos;
		
	}
	
	public boolean missileoutofbounds() { // check if the missile is now out of the Jframe
		
		if (ypos < 0 || ypos + height > GameLayout.GAME_HEIGHT) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public void missilepictures(Graphics input) {
		if (missileupgrade == 0) {
			input.drawImage(ImportImages.missile,xpos ,ypos, 4, 11, null); // draw the missile
		} else if (missileupgrade == 1) {
			input.drawImage(ImportImages.missile, xpos, ypos, 8, 23 , null); // upgraded missile
		} else if (missileupgrade == 2) {
			input.drawImage(ImportImages.missile, xpos, ypos , 12, 34, null); // upgraded missile
		} else if (missileupgrade == 3) {
			input.drawImage(ImportImages.missile, xpos, ypos , 15, 45, null); // upgraded missile
		}
		
	}
	
	public Rectangle getBounds(){
		return new Rectangle(xpos,ypos,3,3);
	}
	

	@SuppressWarnings("static-access")
	public void control() {
		 // move the missile
		if (!isPaused){
			ypos = ypos + missileymovement;
		}
		else{
			ypos = ypos+0;
		}
		// TODO Auto-generated method stub
		
		/* handling for campaign1_1*/
		if (StateMachineControl.outputmainmenucontrol == 1){
			for(int i = 0; i < Campenemies.size(); i++){
				
				Campagin1_1.changehit(0); // no input
				if(getBounds().intersects(Campenemies.get(i).getBounds()) && !isPaused){
					Campagin1_1.changehit(1); // hitbox confirmed
					remove();
					
					if (missileupgrade == 0) {
						Campenemies.get(i).updateLives(-1);
						Campenemies.get(i).updateShieldHealth(1);
						if(Campenemies.get(i).lifeReading() <= 0){
							Campenemies.remove(Campenemies.get(i)); //this removes an enemy if it is hit with the missile
							soundeffects = new  HashMap<String, ImportMusic>();
							soundeffects.put("Blast", new ImportMusic("/Blast.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
							soundeffects.get("Blast").playwav();
						  }
						}
						// draw the missile
					 else if (missileupgrade == 1) {
						Campenemies.get(i).updateLives(-2);
						Campenemies.get(i).updateShieldHealth(2);
						if(Campenemies.get(i).lifeReading() <= 0){
							Campenemies.remove(Campenemies.get(i)); //this removes an enemy if it is hit with the missile
							soundeffects = new  HashMap<String, ImportMusic>();
							soundeffects.put("Blast", new ImportMusic("/Blast.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
							soundeffects.get("Blast").playwav();
						  }
						}
					else if (missileupgrade == 2) {
						Campenemies.get(i).updateLives(-3);
						Campenemies.get(i).updateShieldHealth(3);
						if(Campenemies.get(i).lifeReading() <= 0){
							Campenemies.remove(Campenemies.get(i)); //this removes an enemy if it is hit with the missile
							soundeffects = new  HashMap<String, ImportMusic>();
							soundeffects.put("Blast", new ImportMusic("/Blast.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
							soundeffects.get("Blast").playwav();
						   }
						}
					else if (missileupgrade == 3) {
						Campenemies.get(i).updateLives(-4);
						Campenemies.get(i).updateShieldHealth(4);
						if(Campenemies.get(i).lifeReading() <= 0){
							Campenemies.remove(Campenemies.get(i)); //this removes an enemy if it is hit with the missile
							soundeffects = new  HashMap<String, ImportMusic>();
							soundeffects.put("Blast", new ImportMusic("/Blast.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
							soundeffects.get("Blast").playwav();
						}
					}
					
	
				}


			}				
			//Shield Collision	
			for(int i = 0; i  < Campagin1_1.SH.wall.size(); i++){
				//SurvivalCo_op.SH.wall.remove(i);
				if(getBounds().intersects(Campagin1_1.SH.wall.get(i))){
					Campagin1_1.SH.wall.remove(i);
					soundeffects = new  HashMap<String, ImportMusic>();
					soundeffects.put("Break", new ImportMusic("/Break.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
					soundeffects.get("Break").playwav();
					remove();
					
					
					
					

				}
				
			}
			
			}	
		
		/////////////////////////////////////////////////////////////////////////////////////////
		
		else if(StateMachineControl.outputmainmenucontrol == 8){
			
			for(int i = 0; i < SurvienemiesSolo.size(); i++){
				
				SurvivalSolo.changehit(0); // no input
				if(getBounds().intersects(SurvienemiesSolo.get(i).getBounds()) && !isPaused){
					SurvivalSolo.changehit(1); // hitbox confirmed
					remove();
					SurvienemiesSolo.get(i).updateLives(-1);
					SurvienemiesSolo.get(i).updateShieldHealth(1);
					if(SurvienemiesSolo.get(i).lifeReading() <= 0){
						SurvienemiesSolo.remove(SurvienemiesSolo.get(i)); //this removes an enemy if it is hit with the missile
						soundeffects = new  HashMap<String, ImportMusic>();
						soundeffects.put("Blast", new ImportMusic("/Blast.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
						soundeffects.get("Blast").playwav();
					}
				    
				}
			}
			//Shield Collision	
			for(int i = 0; i  < SurvivalSolo.SH.wall.size(); i++){
				if(getBounds().intersects(SurvivalSolo.SH.wall.get(i))){
					SurvivalSolo.SH.wall.remove(i);
					soundeffects = new  HashMap<String, ImportMusic>();
					soundeffects.put("Break", new ImportMusic("/Break.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
					soundeffects.get("Break").playwav();
					remove();

				}
				
			}
		}
		
		/* Collision for  survival multiplayer*/
		else if (StateMachineControl.outputmainmenucontrol == 9){
			for(int i = 0; i < Survienemies.size(); i++){
				
				SurvivalCo_op.changehit(0); // no input
				if(getBounds().intersects(Survienemies.get(i).getBounds()) && !isPaused){
					//Collission logic goes here
					SurvivalCo_op.changehit(1); // hitbox confirmed
					remove();
					Survienemies.get(i).updateLives(-1);
					Survienemies.get(i).updateShieldHealth(1);
					if(Survienemies.get(i).lifeReading() <= 0){
						Survienemies.remove(Survienemies.get(i)); //this removes an enemy if it is hit with the missile
						soundeffects = new  HashMap<String, ImportMusic>();
						soundeffects.put("Blast", new ImportMusic("/Blast.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
						soundeffects.get("Blast").playwav();
					}
				    
				}
			}
			//Shield Collision	
			for(int i = 0; i  < SurvivalCo_op.SH.wall.size(); i++){
				//Campagin1_1.SH.wall.remove(i);
				if(getBounds().intersects(SurvivalCo_op.SH.wall.get(i))){
					SurvivalCo_op.SH.wall.remove(i);
					soundeffects = new  HashMap<String, ImportMusic>();
					soundeffects.put("Break", new ImportMusic("/Break.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
					soundeffects.get("Break").playwav();
					remove();

				}
				
			}
			
		}
		
		/*Collision for CPU demo*/
		else if (StateMachineControl.outputmainmenucontrol == 10){
			for(int i = 0; i < Demoenemies.size(); i++){
				
				CPUDEMO.changehit(0); // no input
				if(getBounds().intersects(Demoenemies.get(i).getBounds()) && !isPaused){
					//Collission logic goes here
					CPUDEMO.changehit(1); // hitbox confirmed
					remove();
					Demoenemies.get(i).updateLives(-1);
					Demoenemies.get(i).updateShieldHealth(1);
					if(Demoenemies.get(i).lifeReading() <= 0){
						Demoenemies.remove(Demoenemies.get(i)); //this removes an enemy if it is hit with the missile
						soundeffects = new  HashMap<String, ImportMusic>();
						soundeffects.put("Blast", new ImportMusic("/Blast.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
						soundeffects.get("Blast").playwav();
					}
				    
				}
			}
			//Shield Collision	
			for(int i = 0; i  < CPUDEMO.SH.wall.size(); i++){
				//Campagin1_1.SH.wall.remove(i);
				if(getBounds().intersects(CPUDEMO.SH.wall.get(i))){
					CPUDEMO.SH.wall.remove(i);
					soundeffects = new  HashMap<String, ImportMusic>();
					soundeffects.put("Break", new ImportMusic("/Break.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
					soundeffects.get("Break").playwav();
					remove();

				}
				
			}
			
		}
	}
	

	public void setPause(){
		isPaused = true;
	}
	
	public void setResume(){
		isPaused = false;
	}
	
	public static void missileupgrade(int upgrade) {
		missileupgrade = upgrade;
	}
	
	private void remove(){//'teleport' bullet out of screen
		ypos = -50;
	}
	
	

}
