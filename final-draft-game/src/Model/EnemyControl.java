package Model;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import StateMachine.CPUDEMO;
import StateMachine.Campagin1_1;
import StateMachine.MainMenu;
import StateMachine.StateMachineControl;
import StateMachine.SurvivalCo_op;
import StateMachine.SurvivalSolo;
import Centre.GameLayout;
import Centre.ImportImages;
import Centre.ImportMusic;

public class EnemyControl {
	
	private boolean isPaused = false;
	private int xpos, ypos;
	private int enemyWIDTH = 35;
	private int enemyHEIGHT = 40;
	private int temp_speed;
	private LinkedList<EnemyBullet> enemybullet;
	private int speed;
	private int yspeed = 30;
	private int bulletspeed;
	private int lives = 4; //the health of the enemy. It will take 'lives' number of hits by a standard bullet to kill the enemy
	private int shieldlives = 0;
	private int enemyType;
	
	
	/*Constructor without declaring speed (speed=2 default)*/
	public EnemyControl (int x, int y, int type) {
		
		enemybullet = new LinkedList<EnemyBullet>(); // due to dynamic amount of missiles being shot

		speed = 2;
		xpos = x;
		ypos = y;
		enemyType = type;
		
	}
	
	/*Constructor with speed field 's'. Will be used for instantiating enemies in Survival mode.*/
	public EnemyControl(int x, int y, int s, int type){
		enemybullet = new LinkedList<EnemyBullet>(); // due to dynamic amount of missiles being shot

		speed = s;
		xpos = x;
		ypos = y;
		enemyType = type;
		
	}
	
	public void enemypictures(Graphics input) {
		
		if(enemyType == 1){
			input.drawImage(ImportImages.enemy, xpos,ypos, enemyWIDTH, enemyHEIGHT,null);
		}
		else if (enemyType == 2){
			input.drawImage(ImportImages.superalien, xpos,ypos, enemyWIDTH, enemyHEIGHT,null);
		}
		else if (enemyType == 3){
			input.drawImage(ImportImages.enemy3, xpos,ypos, enemyWIDTH, enemyHEIGHT,null);
			
		}
		
		/*sets the colour and font for the hit-marker for the EnemyControl object
		 * It will be displayed on the bottom right hand side of the object
		 */
		input.setColor(Color.GREEN);
		input.setFont(new Font("Lucida Console", Font.PLAIN, 15));
		//input.drawString(new Integer(lives).toString(), xpos + 30, ypos + 30);
		  
		/*START DRAW HEALTHBARS*/
		  int shieldhealth = 20 - (5 * shieldlives); 
		
		  input.drawRect(xpos + 8, ypos - 9, shieldhealth, 3);
		  input.fillRect(xpos + 8, ypos - 9, shieldhealth, 3);
		  
		  if (shieldhealth == 15){
			  input.setColor(Color.YELLOW);
			  input.drawRect(xpos + 8, ypos - 9, shieldhealth, 3);
			  input.fillRect(xpos + 8, ypos - 9, shieldhealth, 3);
		  }
		  
		  if (shieldhealth == 10){
			  input.setColor(Color.ORANGE);
			  input.drawRect(xpos + 8, ypos - 9, shieldhealth, 3);
			  input.fillRect(xpos + 8, ypos - 9, shieldhealth, 3);
		  }
		  
		  if (shieldhealth == 5){
			  input.setColor(Color.RED);
			  input.drawRect(xpos + 8, ypos - 9, shieldhealth, 3);
			  input.fillRect(xpos + 8, ypos - 9, shieldhealth, 3);
		  }
		  
		  if (shieldhealth == 0){
			  input.setColor(Color.RED);
			  input.drawRect(xpos + 8, ypos - 9, shieldhealth, 3);
			  input.fillRect(xpos + 8, ypos - 9, shieldhealth, 3);
		  }
		  /*END: DRAW HEALTHBARS*/
		  
		/*Draws the enemy bullet*/
		for (int i = 0; i < enemybullet.size(); i++) {
			EnemyBullet EB = enemybullet.get(i);
			EB.bulletpictures(input);
		}
	}
	
	/*Control Method*/
	public void control(){
		
		/*START: MODE SPECIFIC CONTROL*/
		if (StateMachineControl.outputmainmenucontrol == 1){//Executed only during campaign
			controlCampaign(); //private Method
		}
		else if (StateMachineControl.outputmainmenucontrol == 8){//executed only during Survival Solo mode
			controlSurvivalSolo(); //private Method
		}
		else if (StateMachineControl.outputmainmenucontrol == 9){//executed only during Survival Co-op mode
			controlSurvivalCoop();
		}
		else if (StateMachineControl.outputmainmenucontrol == 10){
			controlDemo();
		}
		/*END: MODE SPECIFIC CONTROL*/
		
		/*START: COMMON CONTROL FOR ALL PLAY MODES*/	
		xpos += speed;
		if (xpos > GameLayout.GAME_WIDTH - enemyWIDTH || xpos < 0){ //Enemy Speed control
			
			speed = -speed;
			ypos += yspeed;
		}
		

		for (int i = 0; i < enemybullet.size(); i++) {
			EnemyBullet EB = enemybullet.get(i);
			EB.control();
			
			if(EB.bulletoutofbounds() == true) {
				enemybullet.remove(i);
				i--;
			}
		}
		/*END: COMMON CONTROL FOR ALL PLAY MODES*/
	}
	
	/*updateLives will change the number of lives the enemy object has*/
	public void updateLives(int change){
		
		lives += change;
		
	}
	
	public void updateShieldHealth(int change){
		
		shieldlives += change;
	}
	
	public void pause(){
		temp_speed = speed;
		isPaused = true;
		speed = 0;
		yspeed = 0;
		if(!enemybullet.isEmpty()){//only happens if there are missiles on screen to prevent errors
			for (int i = 0; i < enemybullet.size(); i++){
				EnemyBullet EB = enemybullet.get(i);
				EB.setPause();
			}
				EnemyBullet EB = enemybullet.get(0);
				EB.setPause();
		}
	}
	
	public void resume(){
		isPaused = false;
		speed = temp_speed;
		yspeed = 10;
		if(!enemybullet.isEmpty()){
			for (int i = 0; i < enemybullet.size(); i++){
				EnemyBullet EB = enemybullet.get(i);
				EB.setResume();
			}
			}
	}
	
	/*START: GETTER METHODS*/
	public int getSpeed(){
		
		return speed;
	}
	
	public int getXPos(){
		return xpos;
	}
	public int getYPos(){
		return ypos;
	}
	
	public int lifeReading(){
		
		return lives;
	}
	
public int ShieldlifeReading(){
		
		return shieldlives;
	}
	
	public Rectangle getBounds(){
		return new Rectangle(xpos,ypos,enemyWIDTH,enemyHEIGHT);
	}
	/*END GETTER METHODS*/
	
	
	
	/*Private methods*/
	private int randomWithRange(int min, int max){
		int range = (max - min) + 1;        
		return (int)(Math.random() * range) + min;
	}
	
	private void controlCampaign(){//collision control during campaign
		for(int i = 0; i < enemybullet.size(); i++){
			if(enemybullet.get(i).getBounds().intersects(Campagin1_1.playercontrol.getBounds())){
				MainMenu.losesoundeffects.put("Hit", new ImportMusic("/HitSound.wav"));
				MainMenu.losesoundeffects.get("Hit").playwav();
				Campagin1_1.playercontrol.updatelives(-1);
				enemybullet.remove(i);
				i--;
			}
		}
		for(int i = 0; i  < Campagin1_1.SH.wall.size(); i++){ //shield collision with bullet
			for (int j = 0; j < enemybullet.size(); j++){
				if(enemybullet.get(j).getBounds().intersects(Campagin1_1.SH.wall.get(i))){
					Campagin1_1.SH.wall.remove(i);					
					enemybullet.remove(j);
				}
			}
			
		}
		
		if (!isPaused){
			if(xpos == randomWithRange(Campagin1_1.playercontrol.xpos - 40, Campagin1_1.playercontrol.xpos + 40) /*&& enemybullet.size() <= 1*/){ 	
				bulletspeed = 12;
				enemybullet.add(new EnemyBullet(xpos, ypos, bulletspeed));
			}

		}
	}
	
	private void controlSurvivalSolo(){//collision control during SurvivalSolo
		for(int i = 0; i < enemybullet.size(); i++){
			if(enemybullet.get(i).getBounds().intersects(SurvivalSolo.playercontrol.getBounds())){
				MainMenu.losesoundeffects.put("Hit", new ImportMusic("/HitSound.wav"));
				MainMenu.losesoundeffects.get("Hit").playwav();
				SurvivalSolo.playercontrol.updatelives(-1);
				enemybullet.remove(i);
				i--;
			}
		}
		for(int i = 0; i  < SurvivalSolo.SH.wall.size(); i++){ //shield collision with bullet
			for (int j = 0; j < enemybullet.size(); j++){
				if(enemybullet.get(j).getBounds().intersects(SurvivalSolo.SH.wall.get(i))){
					SurvivalSolo.SH.wall.remove(i);					
					enemybullet.remove(j);
				}
			}
			
		}
		
		if (!isPaused){
			if(xpos == randomWithRange(SurvivalSolo.playercontrol.xpos - 40, SurvivalSolo.playercontrol.xpos + 40)){ 	
				bulletspeed = 12;
				enemybullet.add(new EnemyBullet(xpos, ypos, bulletspeed));
			}

		}
	}
	
	private void controlSurvivalCoop(){
		for(int i = 0; i < enemybullet.size(); i++){
			if(enemybullet.get(i).getBounds().intersects(SurvivalCo_op.playercontrol.getBounds())){
				MainMenu.losesoundeffects.put("Hit", new ImportMusic("/HitSound.wav"));
				MainMenu.losesoundeffects.get("Hit").playwav();
				SurvivalCo_op.playercontrol.updatelives(-1);
				enemybullet.remove(i);
				i--;
			}
			else if(enemybullet.get(i).getBounds().intersects(SurvivalCo_op.playercontrol2.getBounds())){
				MainMenu.losesoundeffects.put("Hit", new ImportMusic("/HitSound.wav"));
				MainMenu.losesoundeffects.get("Hit").playwav();
				SurvivalCo_op.playercontrol2.updatelives(-1);
				enemybullet.remove(i);
				i--;
			}
		}
		for(int i = 0; i  < SurvivalCo_op.SH.wall.size(); i++){//shield collision with bullet
			for (int j = 0; j < enemybullet.size(); j++){
				if(enemybullet.get(j).getBounds().intersects(SurvivalCo_op.SH.wall.get(i))){
					SurvivalCo_op.SH.wall.remove(i);					
					enemybullet.remove(j);
				}
			}
			
		}
		
		if (!isPaused){
			if(xpos == randomWithRange(SurvivalCo_op.playercontrol.xpos - 40, SurvivalCo_op.playercontrol.xpos + 40) || xpos == randomWithRange(SurvivalCo_op.playercontrol2.xpos - 40, SurvivalCo_op.playercontrol2.xpos + 40)){ 	
				bulletspeed = 12;
				enemybullet.add(new EnemyBullet(xpos, ypos, bulletspeed));
			}

		}
	}
	
	private void controlDemo(){
		for(int i = 0; i < enemybullet.size(); i++){
			if(enemybullet.get(i).getBounds().intersects(CPUDEMO.playercontrol.getBounds())){
				CPUDEMO.playercontrol.updatelives(-1);
				enemybullet.remove(i);
				i--;
			}
		}
		for(int i = 0; i  < CPUDEMO.SH.wall.size(); i++){ //shield collision with bullet
			for (int j = 0; j < enemybullet.size(); j++){
				if(enemybullet.get(j).getBounds().intersects(CPUDEMO.SH.wall.get(i))){
					CPUDEMO.SH.wall.remove(i);					
					enemybullet.remove(j);
				}
			}
			
		}
		
		if (!isPaused){
			if(xpos == randomWithRange(CPUDEMO.playercontrol.xpos - 40, CPUDEMO.playercontrol.xpos + 40)){ 	
				bulletspeed = 12;
				enemybullet.add(new EnemyBullet(xpos, ypos, bulletspeed));
			}

		}
	}
	
}
