package Model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;

public class Shields {
	/*
	 * This class describes the behavior and attributes of the 4 shields in game.
	 */

	public ArrayList<Rectangle> wall = new ArrayList<Rectangle>();
	
	/*Constructor*/
	public Shields(){
		
		Shield(75, 500);
		Shield(275, 500);
		Shield(475, 500);
		Shield(675, 500);
	}
	
	public int getXposOfWall(int i){
		
		return (int) wall.get(i).getX();
	}
	
	public void paint_Shield(Graphics2D G){
		G.setColor(Color.GREEN);
		for(int i =0; i < wall.size(); i++){
			G.fill(wall.get(i));
		}
	}
	
	public void Shield(int xPos, int yPos)	{
		int wallwidth = 10;
		int x = 0;
		for (int i =0; i < 2; i++){ //2 shield layers
			if((14+ (i * 2) + wallwidth < 22 + wallwidth)){
				row(14 + (i * 2) + wallwidth, xPos - (i * 3), yPos + (i * 3));
				x = (i * 3) + 3;
			}else{ 
			row(22 + wallwidth, xPos - x, yPos + (i * 3));
		   
			}      
	
		}
	
		
	}
	public void row(int rows, int xPos, int yPos){
		for (int i = 0; i < rows; i++){
			Rectangle brick = new Rectangle(xPos + (i * 3), yPos, 3, 3);
			wall.add(brick);
		}
	}



}