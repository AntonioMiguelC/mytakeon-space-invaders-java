package Model;

import java.awt.Graphics;
import java.util.LinkedList;

public class EnemyList {
	
	/*This method will assist in instantiating multiple enemies at once
	 * 
	 * the two essential methods are spawn() and survivalSpawn() which
	 * will create a LinkedList with every element being an EnemyControl object
	 */
	
	LinkedList<EnemyControl> enemy = new LinkedList<EnemyControl>();
	private int speedCount = 2; //used for survival only
	EnemyControl tmp;
	
	public EnemyList(){
		spawn();
		
	}
	
	public void spawn(){
		int first = 80;
		int xEnemy = 80;
		int yEnemy = 60;
		
		for (int c = 0; c < 8; c++){
			instantiateEnemy(new EnemyControl(first,10, 1));
			first += 80;
		}
		/*Positions the enemies on screen*/
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 3; j++)
			{
				instantiateEnemy(new EnemyControl(xEnemy*(i+1),yEnemy*(j+1), 1));
			}
		}
		
	}
	
	/*Spawning method for non-stop survival*/
	public void spawnSurvival() {
		
		int first = 80;
		int xEnemy = 80;
		int yEnemy = 60;
		
		for (int c = 0; c < 8; c++){
			instantiateEnemy(new EnemyControl(first,10, speedCount, randomWithRange(1,3)));
			first += 80;
		}
		/*Positions the enemies on screen*/
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 3; j++)
			{
				instantiateEnemy(new EnemyControl(xEnemy*(i+1),yEnemy*(j+1), speedCount, 1));
			}
		}
		
		for (int i = 0; i < enemy.size(); i++){
			enemy.get(i).updateLives(1);
		}
		speedCount += 3;
		
	}
	
	public void spawnGold(){
		
		int first = 80;
		int xEnemy = 80;
		int yEnemy = 60;
		
		for (int c = 0; c < 8; c++){
			instantiateEnemy(new EnemyControl(first,10, 2));
			first += 80;
		}
		/*Positions the enemies on screen*/
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 3; j++)
			{
				instantiateEnemy(new EnemyControl(xEnemy*(i+1),yEnemy*(j+1), 2));
			}
		}
		
	}
public void spawnBlue(){
		
		int first = 80;
		int xEnemy = 80;
		int yEnemy = 60;
		
		for (int c = 0; c < 8; c++){
			instantiateEnemy(new EnemyControl(first,10, 2));
			first += 80;
		}
		/*Positions the enemies on screen*/
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 3; j++)
			{
				instantiateEnemy(new EnemyControl(xEnemy*(i+1),yEnemy*(j+1), 3));
			}
		}
		
	}
	
	public void spawnRand(){
		
		int first = 80;
		int xEnemy = 80;
		int yEnemy = 60;
		
		for (int c = 0; c < 8; c++){
			instantiateEnemy(new EnemyControl(first,10, randomWithRange(1,3)));
			first += 80;
		}
		/*Positions the enemies on screen*/
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 3; j++)
			{
				instantiateEnemy(new EnemyControl(xEnemy*(i+1),yEnemy*(j+1), randomWithRange(1,3)));
			}
		}
		
	}
	
	public boolean isEmpty(){
		
		return enemy.isEmpty();
		
	}
	
	public void instantiateEnemy(EnemyControl e){
		enemy.add(e);
		
	}
	
	public void removeEnemy(EnemyControl e){
		enemy.remove(e);
	}
	
	public void enemypictures(Graphics g){
		for (int i = 0; i < enemy.size(); i++){
			tmp = enemy.get(i);
			
			tmp.enemypictures(g);
		}
	}
	
	public void control(){
		
		for (int i = 0; i < enemy.size(); i++){
			tmp = enemy.get(i);
			
			tmp.control();
			
		}
	}
	
	public LinkedList<EnemyControl> getEnemies(){ //allows other classes to get an individual enemy from the list
		return enemy;
	}
	
	public void pause(){
		if(!enemy.isEmpty()){
			for (int i = 0; i < enemy.size(); i++){
				tmp = enemy.get(i);
				
				tmp.pause();
			}
		}
		
	}
	
	public void resume(){
		if(!enemy.isEmpty()){
			for (int i = 0; i < enemy.size(); i++){
				tmp = enemy.get(i);
				
				tmp.resume();
			}
			
		}
		
	}
	
	public int getSpeed(){
		
		return tmp.getSpeed();
	}
	
	/*PRIVATE METHODS*/
	private int randomWithRange(int min, int max){
		int range = (max - min) + 1;        
		return (int)(Math.random() * range) + min;
	}
	/*PRIVATE METHODS END*/
	

}
