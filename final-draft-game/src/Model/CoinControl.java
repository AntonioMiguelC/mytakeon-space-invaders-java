package Model;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;


import Centre.GameLayout;
import Centre.ImportImages;

public class CoinControl {
	
	/*
	 * 
	 * 
	 */
	
	private int xpos, ypos;
	private int CoinWidth = 50;
	private int CoinHeight = 50;
	private int temp_speed;
	private int speed = 2;
	
	
	private int lives = 2; //the health of the enemy. It will take 'lives' number of hits by a standard bullet to kill the enemy

	
	
	/*Constructor*/
	public CoinControl (int x, int y) {
		
		xpos = x;
		ypos = y;
		
	}
	
	public void coinpictures(Graphics input) {
		input.drawImage(ImportImages.coin, xpos,ypos, CoinWidth, CoinHeight,null);
		
		input.setColor(Color.WHITE);
		input.setFont(new Font("Lucida Console", Font.PLAIN, 15));
		input.drawString(new Integer(lives).toString(), xpos + 30, ypos + 30);
		
		}
		// TODO Auto-generated method stub
		
	
	public void control(){
				
		xpos += speed;
		
		if (xpos > GameLayout.GAME_WIDTH - CoinWidth || xpos < 0){
			
			speed = -speed;
			
		}
	
	}
	
	/*updateLives will change the number of lives the enemy object has*/
public void updateLives(int change){
		
		lives += change;
	}
	
	public int lifeReading(){
		
		return lives;
	}
	
	public Rectangle getBounds(){
		return new Rectangle(xpos,ypos,CoinWidth,CoinHeight);
	}
	

	public int getSpeed(){
		
		return speed;
	}
	
	public void pause(){
		temp_speed = speed;
		speed = 0;
	
	
	}
	
	public void resume(){
		speed = temp_speed;
		
	
	}
	
	public int getXPos(){
		return xpos;
	}
	public int getYPos(){
		return ypos;
	}
}
