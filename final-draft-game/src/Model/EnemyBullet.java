package Model;

import java.awt.Graphics;
import java.awt.Rectangle;




import Centre.GameLayout;
import Centre.ImportImages;


public class EnemyBullet extends Rectangle {
	
	private static final long serialVersionUID = 1L;
	private int speed;
	public int ypos;
	public int xpos;
	private boolean isPaused = false;
	
	
	public EnemyBullet(int xpos, int ypos, int speed) { // input data and get the movement speed of the missile
		this.speed = speed;
		this.ypos = ypos;
		this.xpos = xpos;
			
	}
	    public boolean bulletoutofbounds() { // check if the missile is now out of the Jframe
		
		if (ypos < 0 || ypos + height > GameLayout.GAME_HEIGHT) {
			return true;
		} else {
			return false;
		}
		
	}
	   public void bulletpictures(Graphics input) {
	   input.drawImage(ImportImages.enemymissile,xpos ,ypos, 4, 11, null); // draw the bullet
			
		}
	
	   public Rectangle getBounds(){
			return new Rectangle(xpos,ypos,4,11);
		}
	   
		public void control() {
			 // move the missile
			if (!isPaused){
				ypos = ypos + speed;
			}
			else{
				ypos = ypos+0;
			}

				
		}
		
		public void setPause(){
			isPaused = true;
		}
		
		public void setResume(){
			isPaused = false;
		}
		private void remove(){//'teleport' bullet out of screen
			ypos = 50;
		}
		
		
}
