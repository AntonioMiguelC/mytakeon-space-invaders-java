package Model;

import java.awt.Graphics;
import java.util.LinkedList;

public class CoinList {
	
	static LinkedList<CoinControl> coin = new LinkedList<CoinControl>();
	CoinControl tmp;
	
	public CoinList(){
		spawn();
		
	}
	
	public void spawn(){
		int first = 80;
		int xEnemy = 80;
		int yEnemy = 60;
		
		for (int c = 0; c < 4; c++){
			instantiateCoin(new CoinControl(first,10));
			first += 80;
		}
		/*Positions the enemies on screen*/
		for (int i = 0; i < 4; i++){
			for (int j = 0; j < 3; j++)
			{
			instantiateCoin(new CoinControl(xEnemy*(i+1),yEnemy*(j+1)));
			}
		}
		
		
	}
	
	public boolean isEmpty(){
		
		return coin.isEmpty();
		
	}
	
	public void instantiateCoin(CoinControl e){
		coin.add(e);
		
	}
	
	public void removeEnemy(CoinControl e){
		coin.remove(e);
	}
	
	public void enemypictures(Graphics g){
		for (int i = 0; i < coin.size(); i++){
			tmp = coin.get(i);
			
			tmp.coinpictures(g);
		}
	}
	
	public void control(){
		
		for (int i = 0; i < coin.size(); i++){
			tmp = coin.get(i);
			
			tmp.control();
			
		}
	}
	
	public LinkedList<CoinControl> getCoins(){ //allows other classes to get an individual enemy from the list
		return coin;
	}
	public static LinkedList<CoinControl> getEnemies(){ //allows other classes to get an individual enemy from the list
		return coin;
	}
	
	public void pause(){
		if(!coin.isEmpty()){
			for (int i = 0; i <  coin.size(); i++){
				tmp = coin.get(i);
				
				tmp.pause();
			}
		}
		
	}
	
	public void resume(){
		if(!coin.isEmpty()){
			for (int i = 0; i < coin.size(); i++){
				tmp = coin.get(i);
				
				tmp.resume();
			}
			
		}
		
	}
	
	public int getSpeed(){
		
		return tmp.getSpeed();
	}
	

}
