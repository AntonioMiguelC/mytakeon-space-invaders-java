package Model;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import StateMachine.Campagin1_1;
import StateMachine.StateMachineControl;
import Centre.GameLayout;
import Centre.ImportImages;
import Centre.ImportMusic;

public class PlayerControl extends Rectangle {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean isPaused = false;
	protected boolean isShooting = false;
	
	protected int lives = 20;

	int rightmovement;
	int leftmovement;
	int xpos;// = 375; // starting position (middle)
	int ypos; //= 550;
	int playerheight = 45;
	int playerwidth = 50;
	int missileshot,multimissileshot;
	int count;
	int prevlocation;
	int missilenumber;
	int missilexpos, missileypos, missileno;
	int missilesize;
	public int multimissilestock;
	private int playerNumber;
	protected LinkedList<Missile> missile;
	private HashMap<String, ImportMusic> soundeffects;
	
	/*Constructor*/
	public PlayerControl(int playerNum) {
		missile = new LinkedList<Missile>(); // due to dynamic amount of missiles being shot
		soundeffects = new  HashMap<String, ImportMusic>();
		soundeffects.put("laser", new ImportMusic("/Laser.wav")); // free sound taken from http://soundbible.com/1087-Laser.html
		
		if (playerNum == 1){
			
			xpos = GameLayout.GAME_WIDTH / 2 - playerwidth/2;
			ypos = 550;
		}
		else{
			xpos = GameLayout.GAME_WIDTH / 4 ;
			ypos = 550;
		}
		
		playerNumber = playerNum;
	}
	

	public void keyPressed(int key) {
		
		if(playerNumber == 1){
			int left = KeyEvent.VK_LEFT; //the spaceship only goes left and right
			int right = KeyEvent.VK_RIGHT;
			int shoot = KeyEvent.VK_SPACE;
			int special = KeyEvent.VK_C;

			
			if (key == left) {
				leftmovement = 1;
				if (key == shoot && !isShooting) {
					isShooting = true;
					missileshot = 1;
				}
			} else if (key == right) {
				rightmovement = 1;
			} else if (key == shoot && !isShooting) { //only one bullet can be shot at a time even when holding spacebar
				isShooting = true;
				missileshot = 1;	
			} else if (key == special && !isShooting) {
				isShooting = true;
				multimissileshot = 1;
			}
		}
		else{
			
			int left = KeyEvent.VK_A; //the spaceship only goes left and right
			int right = KeyEvent.VK_D;
			int shoot = KeyEvent.VK_B;
			int special = KeyEvent.VK_C;

			
			if (key == left) {
				leftmovement = 1;
				if (key == shoot && !isShooting) {
					isShooting = true;
					missileshot = 1;
				}
			} else if (key == right) {
				rightmovement = 1;
			} else if (key == shoot && !isShooting) { //only one bullet can be shot at a time even when holding spacebar
				isShooting = true;
				missileshot = 1;	
			} else if (key == special && !isShooting) {
				isShooting = true;
				multimissileshot = 1;
			}
		}
		
	}

	public void keyReleased(int key) {
		if (playerNumber == 1){
			int left = KeyEvent.VK_LEFT; //the spaceship only goes left and right
			int right = KeyEvent.VK_RIGHT;
			int shoot = KeyEvent.VK_SPACE;
			int special = KeyEvent.VK_C;
			if (key == left) {
				leftmovement = 0; // give no value when person leaves the key
			} else if (key == right) {
				rightmovement = 0;
			} else if (key == shoot) {
				isShooting = false;
				missileshot = 0;
			} else if (key == special) {
				isShooting = false;
				multimissileshot = 0;
			}
		}
		else{
			int left = KeyEvent.VK_A; //the spaceship only goes left and right
			int right = KeyEvent.VK_D;
			int shoot = KeyEvent.VK_B;
			int special = KeyEvent.VK_C;
			if (key == left) {
				leftmovement = 0; // give no value when person leaves the key
			} else if (key == right) {
				rightmovement = 0;
			} else if (key == shoot) {
				isShooting = false;
				missileshot = 0;
			} else if (key == special) {
				isShooting = false;
				multimissileshot = 0;
			}
		}
	
		
	}

	public void control() {
	
		if (isPaused == false){
			missilesize = missile.size();
			if (xpos <= 0) { // if the space ship goes to the left most side
				leftmovement = 0; // stop the movement
			} else if (xpos + playerwidth >= GameLayout.GAME_WIDTH) { // if the space ship goes to the right most side
				rightmovement = 0; // stop the movement
			}  
			if (leftmovement == 1) {
				xpos = xpos - 3; // move left by 3 pixels
			} else if (rightmovement == 1) {
				xpos = xpos + 3; // move right by 3 pixels (change the values if you must to make the player to go faster
			}
			
			if (missileshot == 1) {
				if (missilenumber == 0) {
					missile.add(new Missile(xpos + playerwidth / 2 - 3, ypos - playerheight + 45, -12)); // stock missile
					soundeffects.get("laser").playwav();
					missileshot = 0; // to only make one missile to come out
				} else if (missilenumber == 1) {
					missile.add(new Missile(xpos + playerwidth / 2 - 4, ypos - playerheight + 35, -12)); // upgraded missile
					soundeffects.get("laser").playwav();
					missileshot = 0; // to only make one missile to come out
				} else if (missilenumber == 2) {
					missile.add(new Missile(xpos + playerwidth / 2 - 6, ypos - playerheight + 25, - 12)); // upgraded missile
					soundeffects.get("laser").playwav();
					missileshot = 0; // to only make one missile to come out
				} else if (missilenumber == 3) {
					missile.add(new Missile(xpos + playerwidth / 2 - 7, ypos - playerheight + 15, - 12)); // final version of the missile
					soundeffects.get("laser").playwav();
					missileshot = 0; // to only make one missile to come out
				}
			} else if (multimissileshot == 1) {
				
				if (multimissilestock > 0) {
					missile.add(new Missile(xpos + playerwidth / 2 - 3, ypos - playerheight + 45, -5)); // stock missile centre
					missile.add(new Missile(xpos + playerwidth / 2 - 10, ypos - playerheight + 55, -4)); // stock missile left 
					missile.add(new Missile(xpos + playerwidth / 2 + 4, ypos - playerheight + 55, -4)); // stock missile right
					missile.add(new Missile(xpos + playerwidth / 2 - 20, ypos - playerheight + 65, -3)); // stock missile most left
					missile.add(new Missile(xpos + playerwidth / 2 + 14, ypos - playerheight + 65, -3)); // stock missile most right
					Campagin1_1.usemultimissile(1);
					multimissileshot = 0;
				}
				
				
			}
		}
		else{//When game is paused no player movement happens.
			leftmovement = 0;
			rightmovement = 0;
			missileshot = 0;
		}
		
		
		for (int i = 0; i < missile.size(); i++) {
			Missile mis = missile.get(i);
			mis.control();
			
			
		if(mis.missileoutofbounds() == true) {
				missile.remove(i);
				i = i - 1;
			}
			
			
		}

	}
	 public Rectangle getBounds(){
			return new Rectangle(xpos,ypos,playerwidth,playerheight);
		}

	public void playerpictures(Graphics input) {
		
		if (playerNumber == 1){ //paint the sprite for player 1
			if ((leftmovement == 0) && (rightmovement == 0)) {
				input.drawImage(ImportImages.player, xpos,ypos, playerwidth,playerheight, null); // centre image of the players spaceship
				count = 0; // used for sprites
				prevlocation = 0;
			}
			
			if (leftmovement == 1) {
				count++;	// count the number
				if (prevlocation == 2) {
					count = 0; // reset the count to restart the animation to the left (instead of instant change from right to left)
					//System.out.println(count);
				}
				if (count <= 4) {
				input.drawImage(ImportImages.player4, xpos,ypos, playerwidth,playerheight, null); // slightly tilted
				} else if ((count > 4) && (count <= 8)) {
				input.drawImage(ImportImages.player3, xpos,ypos, playerwidth,playerheight, null); // more tilted
				} else if ((count > 8) && (count <= 12)) {
				input.drawImage(ImportImages.player2, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if (count > 12) {
				input.drawImage(ImportImages.player1, xpos,ypos, playerwidth,playerheight, null); // extremely tilted to the left side
				}
				prevlocation = 1;
			} 
			else if (rightmovement == 1) {
				count++;
				if (prevlocation == 1) {
					count = 0; // reset the count to restart the animation to the right (instead of instant change from left to right)
					//System.out.println(count);
				}
				if (count <= 4) {
				input.drawImage(ImportImages.player6, xpos,ypos, playerwidth,playerheight, null); // sightly tilted
				} else if ((count > 4) && (count <= 8)) {
				input.drawImage(ImportImages.player7, xpos,ypos, playerwidth,playerheight, null); // more tilted
				} else if ((count > 8) && (count <= 12)) {
				input.drawImage(ImportImages.player8, xpos,ypos, playerwidth,playerheight, null); // tilted	
				} else if (count > 12) {
				input.drawImage(ImportImages.player9, xpos,ypos, playerwidth,playerheight, null); // extremely tilted to the right side	
				}
				prevlocation = 2;
			}
			
		}
		else{
			if ((leftmovement == 0) && (rightmovement == 0)) {
				input.drawImage(ImportImages.secondplayer11, xpos,ypos, playerwidth,playerheight, null); // centre image of the players spaceship
				count = 0; // used for sprites
				prevlocation = 0;
			}
			else if (leftmovement == 1) {
				
				if (prevlocation == 2) {
					count = 0; // reset the count to restart the animation to the left (instead of instant change from right to left)
				}
				count++;	// count the number
				if (count <= 2) {
				input.drawImage(ImportImages.secondplayer12, xpos,ypos, playerwidth,playerheight, null); // slightly tilted
				} else if ((count > 2) && (count <= 4)) {
				input.drawImage(ImportImages.secondplayer13, xpos,ypos, playerwidth,playerheight, null); // more tilted
				} else if ((count > 4) && (count <= 6)) {
				input.drawImage(ImportImages.secondplayer14, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 6) && (count <= 8)) {
				input.drawImage(ImportImages.secondplayer15, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 8) && (count <= 10)) {
				input.drawImage(ImportImages.secondplayer16, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 10) && (count <= 12)) {
				input.drawImage(ImportImages.secondplayer17, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 12) && (count <= 14)) {
				input.drawImage(ImportImages.secondplayer18, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 14) && (count <= 16)) {
				input.drawImage(ImportImages.secondplayer19, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 16) && (count <= 18)) {
				input.drawImage(ImportImages.secondplayer20, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if (count > 18) {
				input.drawImage(ImportImages.secondplayer21, xpos,ypos, playerwidth,playerheight, null); // tilted to the most left
				prevlocation = 1;
				}
			} else if (rightmovement == 1) {
				
				if (prevlocation == 1) {
					count = 0; // reset the count to restart the animation to the right (instead of instant change from left to right)
				}
				count++;
				if (count <= 1) {
				input.drawImage(ImportImages.secondplayer10, xpos,ypos, playerwidth,playerheight, null); // slightly tilted
				} else if ((count > 1) && (count <= 2)) {
				input.drawImage(ImportImages.secondplayer9, xpos,ypos, playerwidth,playerheight, null); // more tilted
				} else if ((count > 2) && (count <= 3)) {
				input.drawImage(ImportImages.secondplayer8, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 3) && (count <= 4)) {
				input.drawImage(ImportImages.secondplayer7, xpos,ypos, playerwidth,playerheight, null); // more tilted
				} else if ((count > 4) && (count <= 5)) {
				input.drawImage(ImportImages.secondplayer6, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 5) && (count <= 6)) {
				input.drawImage(ImportImages.secondplayer5, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 6) && (count <= 7)) {
				input.drawImage(ImportImages.secondplayer4, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 7) && (count <= 8)) {
				input.drawImage(ImportImages.secondplayer3, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if ((count > 9) && (count <= 10)) {
				input.drawImage(ImportImages.secondplayer2, xpos,ypos, playerwidth,playerheight, null); // tilted
				} else if (count > 10) {
				input.drawImage(ImportImages.secondplayer1, xpos,ypos, playerwidth,playerheight, null); // tilted to the most right
				prevlocation = 2;
				}
			}
		}
		
		for (int i = 0; i < missile.size(); i++) {
			Missile mis = missile.get(i);
			mis.missilepictures(input);
		}	
		
	}
	
	public void updatelives(int numLives){
		
		lives += numLives;
		
	}
	
	public int getLivesAsInt(){
		return lives;
	}
	
	public String getLivesAsString(){
		
		return new Integer(lives).toString();
	}
	
	public void pause(){ //to be done when player presses 'pause' key
		isPaused = true;//used for determining pause state
		
		if(!missile.isEmpty()){//only happens if there are missiles on screen to prevent errors
			for (int i = 0; i < missile.size(); i++){
				Missile mis = missile.get(i);
				mis.setPause();
			}
			Missile mis = missile.get(0);
			mis.setPause();
		}

	}
	
	public void resume(){ //to be done if screen is paused and player presses 'pause' key
		
		isPaused = false;
		if(!missile.isEmpty()){
			for (int i = 0; i < missile.size(); i++){
				Missile mis = missile.get(i);
				mis.setResume();
			}
		}
	}
	
	public void remove(){
		
		xpos = 1000;
		ypos = 1000;
	}
	
	
	public void playermissileupgrade(int upgrade) {
		missilenumber = upgrade;
		Missile.missileupgrade(upgrade);
		
	}
	
	public void playermultimissilepurchase(int purchase) {
		multimissilestock = purchase;
	}

}