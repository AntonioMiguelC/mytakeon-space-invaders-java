package Model;

import java.util.LinkedList;

import StateMachine.CPUDEMO;
import Centre.GameLayout;

public class AutonomousPlayerControl extends PlayerControl {
	/*
	 * This class is a child class of the normal PlayerControl class.
	 * This class will move and shoot autonomously
	 */

	
	private static final long serialVersionUID = 1L;
	
	LinkedList<EnemyControl> enemies;

	public AutonomousPlayerControl(int playerNum) {
		super(playerNum);
		lives = 100;
		
		
	}
	
	public void keyPressed(int key) {
		
	}
	public void keyReleased(int key) {
		
	}
	
	public void autonomousControl(){
		
		enemies = CPUDEMO.enemies.getEnemies();
		
		//Auto Control for shooting
		if(!missile.isEmpty()){
			if(missile.getFirst().ypos < GameLayout.GAME_HEIGHT / 2 && noShieldAbove()){//shoot only if
																						//there is no shield
				isShooting = true;														//above
				missileshot = 1;
			}
			else{
				isShooting = false;
				missileshot = 0;
			}
			
		}
		else{
			
			if(missile.size() <= 1 && noShieldAbove()){
				isShooting = true;
				missileshot = 1;
			}
			else{
				isShooting = false;
				missileshot = 0;
			}
		}
		
		//Auto control for movement
		if (!enemies.isEmpty()){
			if(leftmovement == 0 && rightmovement == 0){//initalise movement
				leftmovement = 0;
				rightmovement = 1;
			}
			else if (enemies.size() > 1){ //as long as there is more than 1 enemy
				//keep track of screen boundary and keep track on a specific target's x position
				//and move accordingly
				if (xpos >= GameLayout.GAME_WIDTH - playerwidth || (xpos >= enemies.get(enemies.size() / 6).getXPos() + 40)){
					leftmovement = 1;
					rightmovement = 0;
				}
				else if(xpos <= 0 || (xpos <= enemies.get(enemies.size() / 6).getXPos() - 40)){
					rightmovement = 1;
					leftmovement = 0;
				}
			}
			else if (enemies.size() == 1){
				//If there is only one enemy, keep track of that enemy's position and move accordingly
				if (xpos >= GameLayout.GAME_WIDTH - playerwidth || (xpos >= enemies.get(0).getXPos() + 40)){
					leftmovement = 1;
					rightmovement = 0;
				}
				else if(xpos <= 0 || (xpos < enemies.get(0).getXPos())){
					rightmovement = 1;
					leftmovement = 0;
				}
			}
		}
		else{
			leftmovement = 0;
			rightmovement = 0;
	
		}
			
	}
	
	/*Private Methods*/
	private boolean noShieldAbove(){// we know the positions of the shield.
									//check that the player has no shield above them
		if ((xpos >= 75 - playerwidth / 2 && xpos < 130) || (xpos >= 275 - playerwidth / 2 && xpos < 330) || (xpos >= 475 - playerwidth / 2 && xpos < 530) || (xpos >= 675 - playerwidth / 2 && xpos < 730)){
			
			return false;
		}
		else{
			return true;
		}
		
	}
	
	

}
